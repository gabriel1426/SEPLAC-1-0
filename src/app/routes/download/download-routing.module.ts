import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DownloadDownloadFileComponent } from './download-file/download-file.component';

const routes: Routes = [{ path: 'download-file', component: DownloadDownloadFileComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DownloadRoutingModule { }
