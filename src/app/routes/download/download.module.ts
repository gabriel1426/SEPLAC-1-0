import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { DownloadRoutingModule } from './download-routing.module';
import { DownloadDownloadFileComponent } from './download-file/download-file.component';

const COMPONENTS = [DownloadDownloadFileComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    DownloadRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_DYNAMIC
  ],
  entryComponents: COMPONENTS_DYNAMIC
})
export class DownloadModule { }
