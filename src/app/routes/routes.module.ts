import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { RoutesRoutingModule } from './routes-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './sessions/login/login.component';
import { RegisterComponent } from './sessions/register/register.component';
import { AuthGuardDocentesServiceService } from '../core/AuthGuard/auth-guard-docentes-service.service';
import { AuthGuardAdministrativoService } from '../core/AuthGuard/auth-guard-administrativo.service';
import { AuthGuardDashboardService } from '../core/AuthGuard/auth-guard-dashboard.service';
import { AuthGuardDirectorProgramaService } from '../core/AuthGuard/auth-guard-director-programa.service';

const COMPONENTS = [DashboardComponent, LoginComponent, RegisterComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, RoutesRoutingModule],
  providers: [
    AuthGuardDocentesServiceService,
    AuthGuardAdministrativoService,
    AuthGuardDashboardService,
    AuthGuardDirectorProgramaService,
  ],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class RoutesModule {}
