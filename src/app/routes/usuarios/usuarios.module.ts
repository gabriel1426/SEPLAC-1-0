import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { UsuariosPerfilComponent } from './perfil/perfil.component';
import { DataTablesModule } from 'angular-datatables';

const COMPONENTS = [UsuariosListarUsuariosComponent, UsuariosPerfilComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    UsuariosRoutingModule,
    DataTablesModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_DYNAMIC
  ],
  entryComponents: COMPONENTS_DYNAMIC
})
export class UsuariosModule { }
