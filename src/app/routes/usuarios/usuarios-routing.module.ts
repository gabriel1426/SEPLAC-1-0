import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { UsuariosPerfilComponent } from './perfil/perfil.component';

const routes: Routes = [{ path: 'listar-usuarios', component: UsuariosListarUsuariosComponent },
{ path: 'perfil', component: UsuariosPerfilComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
