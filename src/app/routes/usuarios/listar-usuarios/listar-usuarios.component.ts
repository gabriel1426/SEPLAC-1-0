import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-usuarios-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.scss'],
})
export class UsuariosListarUsuariosComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: any = new Subject();
  public item: any;
  public programaAcademicos: any;
  Usuarios: any;
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  registrarUsuario: FormGroup;
  modificarUsuario: FormGroup;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.dtOptions = {
      ajax: 'data/data.json',

      // Use this attribute to enable the responsive extension
      responsive: true,
    };
    this.registrarUsuario = this.fb.group({
      codigo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });

    this.modificarUsuario = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });
    this.tabla();
    this.listarUsuarios();
    this.listarProgramaAcademico();
  }

  getErrorMessage(form: FormGroup) {
    return form.get('email').hasError('required')
      ? 'Por favor ingresa el correo'
      : form.get('email').hasError('email')
      ? 'Correo no valido'
      : '';
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(fX_ iltrado de _MAtotal entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarUsuario() {
    this.dialog.open(this.agregar);
  }

  modificarUsuarioDialog(item: any) {
    this.item = item;
    this.modificarUsuario.controls.codigo.setValue(item.codigo);
    this.modificarUsuario.controls.email.setValue(item.email);
    this.modificarUsuario.controls.nombre.setValue(item.name);
    this.modificarUsuario.controls.apellido.setValue(item.apellidos);
    this.modificarUsuario.patchValue({ contrato: item.contrato });
    this.modificarUsuario.patchValue({ programa_academico_id: item.programa_academico_id });
    this.dialog.open(this.modificar);
  }

  EliminarUsuarioDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }
  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }

  listarUsuarios() {
    let url = '/api/rol/Usuario';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.Usuarios = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisUsuario() {
    let data = {
      rol_id: 3,
      name: this.registrarUsuario.controls.nombre.value,
      apellidos: this.registrarUsuario.controls.apellido.value,
      codigo: this.registrarUsuario.controls.codigo.value,
      email: this.registrarUsuario.controls.email.value,
      contrato: this.registrarUsuario.controls.contrato.value,
      programa_academico_id: this.registrarUsuario.controls.programa_academico_id.value,
    };
    let url = '/api/users';
    if (this.registrarUsuario.valid) {
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarUsuario.reset();
            this.listarUsuarios();
            this.dialog.closeAll();
          } else {
            this.registroFallido(data.message);
          }
        },
        error => {
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiUsuario() {
    let data = {
      rol_id: 3,
      name: this.modificarUsuario.controls.nombre.value,
      apellidos: this.modificarUsuario.controls.apellido.value,
      codigo: this.modificarUsuario.controls.codigo.value,
      email: this.modificarUsuario.controls.email.value,
      contrato: this.modificarUsuario.controls.contrato.value,
      programa_academico_id: this.modificarUsuario.controls.programa_academico_id.value,
    };
    let url = '/api/users/' + this.item.codigo;
    if (this.modificarUsuario.valid) {
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarUsuarios();
            this.dialog.closeAll();
          } else {
            this.registroFallido(data.message);
          }
        },
        error => {
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarUsuario() {
    let url = '/api/users/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Usuario eliminado exitosamente!!');
          this.listarUsuarios();
          this.dialog.closeAll();
        } else {
          this.registroFallido(data.message);
        }
      },
      error => {
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
