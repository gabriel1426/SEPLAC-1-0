import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { DirectorProgramaRoutingModule } from './director-programa-routing.module';
import { DirectorProgramaListarDirectoresComponent } from './listar-directores/listar-directores.component';
import { DataTablesModule } from 'angular-datatables';

const COMPONENTS = [DirectorProgramaListarDirectoresComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, DirectorProgramaRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class DirectorProgramaModule {}
