import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-director-programa-listar-directores',
  templateUrl: './listar-directores.component.html',
  styleUrls: ['./listar-directores.component.scss'],
})
export class DirectorProgramaListarDirectoresComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public programaAcademicos: any;
  public DirectorPrograma: any;
  public cargar = false;
  public usuario;
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  registrarDirector: FormGroup;
  modificarDirector: FormGroup;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.dtOptions = {
      ajax: 'data/data.json',

      // Use this attribute to enable the responsive extension
      responsive: true,
    };
    this.registrarDirector = this.fb.group({
      codigo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });

    this.modificarDirector = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });
    this.tabla();
    this.usuario = this.provider.obtenerUser();
    this.listarDirectorPrograma();
    this.listarProgramaAcademico();
  }

  getErrorMessage(form: FormGroup) {
    return form.get('email').hasError('required')
      ? 'Por favor ingresa el correo'
      : form.get('email').hasError('email')
      ? 'Correo no valido'
      : '';
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(fX_ iltrado de _MAtotal entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarDirector() {
    if (this.programaAcademicos == undefined || this.programaAcademicos.length == 0) {
      this.registroFallido('No hay programas academicos registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarDirectorProgramaDialog (item: any) {
    this.item = item;
    this.modificarDirector.controls.codigo.setValue(item.codigo);
    this.modificarDirector.controls.email.setValue(item.email);
    this.modificarDirector.controls.nombre.setValue(item.name);
    this.modificarDirector.controls.apellido.setValue(item.apellidos);
    this.modificarDirector.patchValue({ contrato: item.contrato });
    this.modificarDirector.patchValue({ programa_academico_id: item.programa_academico_id });
    this.dialog.open(this.modificar);
  }

  EliminarDirectorProgramaDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }
  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }

  listarDirectorPrograma() {
    let url = '/api/rol/4';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.DirectorPrograma = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisDirectorPrograma() {
    if (this.registrarDirector.valid) {
      this.cargar = true;
      let data = {
        rol_id: 4,
        name: this.registrarDirector.controls.nombre.value,
        apellidos: this.registrarDirector.controls.apellido.value,
        codigo: this.registrarDirector.controls.codigo.value,
        email: this.registrarDirector.controls.email.value,
        contrato: this.registrarDirector.controls.contrato.value,
        programa_academico_id: this.registrarDirector.controls.programa_academico_id.value,
      };
      let url = '/api/users';

      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarDirector.reset();
            this.listarDirectorPrograma();
            this.cargar = false;
            this.dialog.closeAll();
            this.cargar = false;
          } else {
            this.registroFallido(data.message);
            this.cargar = false;
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiDirectorPrograma() {
    if (this.modificarDirector.valid) {
      this.cargar = true;
      let data = {
        rol_id: 4,
        name: this.modificarDirector.controls.nombre.value,
        apellidos: this.modificarDirector.controls.apellido.value,
        codigo: this.modificarDirector.controls.codigo.value,
        email: this.modificarDirector.controls.email.value,
        contrato: this.modificarDirector.controls.contrato.value,
        programa_academico_id: this.modificarDirector.controls.programa_academico_id.value,
      };
      let url = '/api/users/' + this.item.id;

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarDirectorPrograma();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarDirectorPrograma() {
    this.cargar = true;
    let url = '/api/users/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Director eliminado exitosamente!!');
          this.listarDirectorPrograma();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
