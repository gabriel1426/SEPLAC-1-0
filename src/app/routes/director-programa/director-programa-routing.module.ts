import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectorProgramaListarDirectoresComponent } from './listar-directores/listar-directores.component';

const routes: Routes = [{ path: '', component: DirectorProgramaListarDirectoresComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DirectorProgramaRoutingModule {}
