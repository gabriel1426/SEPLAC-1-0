import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-facultad-listasFacultad',
  templateUrl: './listas-facultad.component.html',
  styleUrls: ['./listas-facultad.component.scss'],
})
export class FacultadListasFacultadComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public facultades: any;
  public registrarFacultad: FormGroup;
  public modificarFacultad: FormGroup;
  public cargar = false;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarFacultad = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      //descripcion: ['', [Validators.required]],
    });

    this.modificarFacultad = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      // descripcion: ['', [Validators.required]],
    });
    this.tabla();
    this.listarFacultades();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      autoWidth: true,
      pagingType: 'full_numbers',
      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarFacultad() {
    this.dialog.open(this.agregar);
  }

  modificarFacultadDialog(item: any) {
    this.item = item;
    this.modificarFacultad.controls.codigo.setValue(item.codigo);
    this.modificarFacultad.controls.codigo.disable;
    this.modificarFacultad.controls.nombre.setValue(item.nombre);
    this.dialog.open(this.modificar);
  }
  EliminarFacultadDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarFacultades() {
    let url = '/api/facultad';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.facultades = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisFacultad() {
    if (this.registrarFacultad.valid) {
      this.cargar = true;
      let data = {
        facultades: [
          {
            nombre: this.registrarFacultad.controls.nombre.value,
            codigo: this.registrarFacultad.controls.codigo.value,
          },
        ],
      };
      let url = '/api/facultad';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarFacultad.reset();
            this.listarFacultades();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifyFacultad(item: any) {
    if (this.modificarFacultad.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarFacultad.controls.codigo.value,
        nombre: this.modificarFacultad.controls.nombre.value,
      };
      let url = '/api/facultad/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarFacultades();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarFacultad() {
    this.cargar = true;
    let url = '/api/facultad/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Facultad eliminada exitosamente!!');
          this.listarFacultades();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
