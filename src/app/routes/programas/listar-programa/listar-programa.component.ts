import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-programas-listar-programa',
  templateUrl: './listar-programa.component.html',
  styleUrls: ['./listar-programa.component.scss'],
})
export class ProgramasListarProgramaComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public lineas: any;
  public item: any;
  public programas: any;
  public cargar = false;
  public registrarPrograma: FormGroup;
  public modificarPrograma: FormGroup;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarPrograma = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      linea: ['', [Validators.required]],
    });

    this.modificarPrograma = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      linea: ['', [Validators.required]],
    });
    this.tabla();
    this.listarProgramas();
    this.listarLinea();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  listarLinea() {
    let url = '/api/linea';
    this.provider.listar(url).subscribe(
      data => {
        this.lineas = data.data;
      },
      error => {}
    );
  }

  agregarPrograma() {
    if (this.lineas == undefined || this.lineas.length == 0) {
      this.registroFallido('No hay líneas estratégicas registradas');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarProgramaDialog(item: any) {
    this.item = item;
    this.modificarPrograma.controls.codigo.setValue(item.codigo);
    this.modificarPrograma.controls.nombre.setValue(item.nombre);
    this.modificarPrograma.controls.descripcion.setValue(item.descripcion);
    this.modificarPrograma.patchValue({ linea: item.linea.id });
    this.dialog.open(this.modificar);
  }

  EliminarProgramaDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarProgramas() {
    let url = '/api/programa';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.programas = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisPrograma() {
    if (this.registrarPrograma.valid) {
      this.cargar = true;
      let data = {
        linea_id: this.registrarPrograma.controls.linea.value,
        programas: [
          {
            nombre: this.registrarPrograma.controls.nombre.value,
            codigo: this.registrarPrograma.controls.codigo.value,
            descripcion: this.registrarPrograma.controls.descripcion.value,
          },
        ],
      };
      let url = '/api/programa';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarPrograma.reset();
            this.listarProgramas();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifyPrograma(item: any) {
    if (this.modificarPrograma.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarPrograma.controls.codigo.value,
        nombre: this.modificarPrograma.controls.nombre.value,
        descripcion: this.modificarPrograma.controls.descripcion.value,
        linea_id: this.modificarPrograma.controls.linea.value,
      };
      let url = '/api/programa/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarProgramas();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }

  eliminarPrograma() {
    this.cargar = true;
    let url = '/api/programa/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Programa eliminado exitosamente!!');
          this.listarProgramas();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
