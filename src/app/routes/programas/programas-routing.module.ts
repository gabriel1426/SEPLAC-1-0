import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramasListarProgramaComponent } from './listar-programa/listar-programa.component';

const routes: Routes = [{ path: '', component: ProgramasListarProgramaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramasRoutingModule {}
