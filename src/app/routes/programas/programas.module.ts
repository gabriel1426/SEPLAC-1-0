import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { ProgramasRoutingModule } from './programas-routing.module';
import { ProgramasListarProgramaComponent } from './listar-programa/listar-programa.component';
import { DataTablesModule } from 'angular-datatables';

const COMPONENTS = [ProgramasListarProgramaComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, ProgramasRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class ProgramasModule {}
