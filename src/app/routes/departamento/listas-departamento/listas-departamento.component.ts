import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-departamento-listasDepartamento',
  templateUrl: './listas-departamento.component.html',
  styleUrls: ['./listas-departamento.component.scss'],
})
export class DepartamentoListasDepartamentoComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public facultades: any;
  public cargar = false;
  public departamentos: any;
  public registrarDepartamento: FormGroup;
  public modificarDepartamento: FormGroup;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarDepartamento = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      facultad: ['', [Validators.required]],
    });

    this.modificarDepartamento = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      facultad: ['', [Validators.required]],
    });
    this.listarDepartamentos();
    this.listarFacultades();
    this.tabla();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarDepartamento() {
    if (this.facultades == undefined || this.facultades.length == 0) {
      this.registroFallido('No hay facultades registradas');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarDepartamentoDialog(item: any) {
    this.item = item;
    this.modificarDepartamento.controls.codigo.setValue(item.codigo);
    this.modificarDepartamento.controls.nombre.setValue(item.nombre);
    this.modificarDepartamento.patchValue({ facultad: item.facultad_id });
    this.dialog.open(this.modificar);
  }

  EliminarDepartamentoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarFacultades() {
    let url = '/api/facultad';
    this.provider.listar(url).subscribe(
      data => {
        this.facultades = data.data;
      },
      error => {}
    );
  }
  listarDepartamentos() {
    let url = '/api/departamento';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.departamentos = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisDepartamento() {
    if (this.registrarDepartamento.valid) {
      this.cargar = true;
      let data = {
        departamentos: [
          {
            codigo: this.registrarDepartamento.controls.codigo.value,
            nombre: this.registrarDepartamento.controls.nombre.value,
          },
        ],
        facultad_id: this.registrarDepartamento.controls.facultad.value,
      };
      let url = '/api/departamento';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarDepartamento.reset();
            this.listarDepartamentos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiDepartamento(item: any) {
    if (this.modificarDepartamento.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarDepartamento.controls.codigo.value,
        nombre: this.modificarDepartamento.controls.nombre.value,
        facultad_id: this.modificarDepartamento.controls.facultad.value,
      };
      let url = '/api/departamento/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarDepartamentos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarDepartamento() {
    this.cargar = true;
    let url = '/api/departamento/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Departamento eliminado exitosamente!!');
          this.listarDepartamentos();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
