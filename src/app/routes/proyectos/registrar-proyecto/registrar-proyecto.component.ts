import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';


@Component({
  selector: 'app-proyectos-registrar-proyecto',
  templateUrl: './registrar-proyecto.component.html',
  styleUrls: ['./registrar-proyecto.component.scss']
})
export class ProyectosRegistrarProyectoComponent {

  registrarProyecto: FormGroup;
  programas:any;
  NombreProgramaDocente;

  constructor( private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,) {
    this.registrarProyecto = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      objetivo: ['', [Validators.required]],
      programa: ['', [Validators.required]],
      selectPrograma: ['', [Validators.required]],
      NombreProgramaDocente: [{ value: '', disabled: true }, [Validators.required]]
    });
    this.consultarProgramaAcademico();
    this.listarProgramas();
  }

  
  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }
  select(item){
  }

  consultarProgramaAcademico(){
    let usuario:any=this.provider.obtenerUser();
    if(usuario.rol_id==3){
     let url="/api/programa_academico/"+usuario.programa_academico_id;
      this.provider.listar(url).subscribe(
        data => {
          this.registrarProyecto.controls.programa.setValue(usuario.programa_academico_id);
          this.registrarProyecto.controls.NombreProgramaDocente.setValue(data.data.nombre);
        },
        error => {}
      );
     
    }
  }
  listarProgramas() {
    let url = '/api/programa';
    this.provider.listar(url).subscribe(
      data => {
       
          this.programas = data.data;
          
      },
      error => {}
    );
  }

  regisProyecto() {
    let data = {
          
          nombre: this.registrarProyecto.controls.nombre.value,
          descripcion: this.registrarProyecto.controls.descripcion.value,
          objetivo: this.registrarProyecto.controls.objetivo.value,
          programa_academico_id: this.registrarProyecto.controls.programa.value,
          programas: this.registrarProyecto.controls.selectPrograma.value,
    };
    let url = '/api/proyecto';
    if (this.registrarProyecto.valid) {
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarProyecto.reset();
          } else {
            this.registroFallido(data.message);
          }
        },
        error => {
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }
}
