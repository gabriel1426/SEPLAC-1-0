import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-proyectos-listar-proyecto',
  templateUrl: './listar-proyecto.component.html',
  styleUrls: ['./listar-proyecto.component.scss'],
})
export class ProyectosListarProyectoComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  // fechas
  public departDate: Date;
  public returnDate: Date;
  public minTripDate = new Date();
  public maxTripDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
  public startTripDate = new Date(Date.now() + 31 * 24 * 60 * 60 * 1000);
  //////
  public programas: any;
  public item: any;
  public idPrograma;
  public proyectos: any;
  public cargar = false;
  public usuario: any;
  public registrarProyecto: FormGroup;
  public modificarProyecto: FormGroup;
  public ejes;
  public ejeSelected;
  public lineas;
  public lineaSelected;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.registrarProyecto = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      objetivo: ['', [Validators.required]],
      programa: ['', [Validators.required]],
      selectEje: ['', [Validators.required]],
      selectLinea: ['', [Validators.required]],
      selectPrograma: ['', [Validators.required]],
      NombreProgramaDocente: [{ value: '', disabled: true }, [Validators.required]],
    });

    this.modificarProyecto = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      objetivo: ['', [Validators.required]],
      programa: [{ value: '', disabled: true }, [Validators.required]],
      selectPrograma: ['', [Validators.required]],
    });
    this.usuario = this.provider.obtenerUser();
    this.consultarProgramaAcademico();
    this.listarEjes();
    this.listarProyectos();
    this.tabla();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }

  agregarProyecto() {
    if (this.ejes == undefined) {
      this.registroFallido('No hay ejes registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }
  abrirReporte(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.planes_proyectos[0].id,
      },
    };
    this.router.navigate(['/reportes/proyecto-general-reporte'], navigationExtras);
  }

  modificarProyectoDialog(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.id,
        idPlan: 0,
        isPlan: false,
        isPlanFin: true,
      },
    };
    this.router.navigate(['/proyectos/modificar-proyecto'], navigationExtras);
  }

  EliminarProyectoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarProyectos() {
    let url;
    if (this.usuario.rol_id == 1 || this.usuario.rol_id == 2) {
      url = '/api/proyecto';
    } else {
      url = '/api/proyecto/getProgramaAcademico/' + this.usuario.programa_academico_id;
    }

    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.proyectos = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  listarEjes() {
    let url = '/api/eje';
    this.provider.listar(url).subscribe(
      data => {
        this.ejes = data.data;
      },
      error => {}
    );
  }

  consultarProgramaAcademico() {
    let usuario: any = this.provider.obtenerUser();
    if (usuario.rol_id == 3 || usuario.rol_id == 4) {
      let url = '/api/programa_academico/' + usuario.programa_academico_id;
      this.provider.listar(url).subscribe(
        data => {
          this.registrarProyecto.controls.programa.setValue(usuario.programa_academico_id);
          this.registrarProyecto.controls.NombreProgramaDocente.setValue(data.data.nombre);
        },
        error => {}
      );
    }
  }

  regisProyecto() {
    if (this.registrarProyecto.valid) {
      this.cargar = true;
      let data = {
        nombre: this.registrarProyecto.controls.nombre.value,
        descripcion: this.registrarProyecto.controls.descripcion.value,
        objetivo: this.registrarProyecto.controls.objetivo.value,
        programa_academico_id: this.registrarProyecto.controls.programa.value,
        programas: this.registrarProyecto.controls.selectPrograma.value,
        usuario_id: this.usuario.id,
      };
      let url = '/api/proyecto';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarProyecto.reset();
            this.listarProyectos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modiProyecto(item: any) {
    if (this.modificarProyecto.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarProyecto.controls.codigo.value,
        nombre: this.modificarProyecto.controls.nombre.value,
        descripcion: this.modificarProyecto.controls.descripcion.value,
      };
      let url = '/api/eje/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarProyectos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarProyecto() {
    this.cargar = true;
    let url = '/api/proyecto/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Proyecto eliminado exitosamente!!');
          this.listarProyectos();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  llenarLineas() {
    this.lineas = this.ejeSelected.lineas;
    this.programas = [];
  }

  llenarProgramas() {
    this.programas = this.lineaSelected.programas;
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
