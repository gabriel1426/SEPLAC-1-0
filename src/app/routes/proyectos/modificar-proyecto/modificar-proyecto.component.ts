import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ViewChildren,
  TemplateRef,
  QueryList,
} from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-proyectos-modificar-proyecto',
  templateUrl: './modificar-proyecto.component.html',
  styleUrls: ['./modificar-proyecto.component.scss'],
})
export class ProyectosModificarProyectoComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild('agregar_responsables', { static: false }) agregar_responsables: TemplateRef<any>;
  @ViewChild('eliminar_responsables', { static: false }) eliminar_responsables: TemplateRef<any>;
  @ViewChildren(DataTableDirective) dtElements: QueryList<DataTableDirective>;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: Subject<any> = new Subject();
  public proyecto: FormGroup;
  public usuario: any;
  public registroActividad: FormGroup;
  public modificarActividad: FormGroup;
  public formSeguimiento: FormGroup;
  public programas: any;
  public indicadores: any;
  public responsables: any;
  public minDate: Date = new Date();
  //Formulario para registra un responsable
  public registroResponsable: FormGroup;
  //Lista de todos los posibles responsables
  public listaResponsables: any;
  //Lista de los responsables de un proyecto
  public responsables_seleccionados: any;
  //Variable usaba para saber si el usuario logeado fue es responsable del proyecto
  public IsResponsable = false;
  //Variable usada para saber si un proyecto se encuentra en un plan vijente y poder o no modificarlo
  public IsModificar = false;
  public recursos: any;
  public periodos = [];
  public periodosTotales: any;
  public periodoMaximo: any;
  public titulo = 'Modifica Proyecto';
  public item: any;
  public id;
  public idPlan;
  // false el plan no ha terminao / true el plan ya termino
  public isPlanFin: any;
  public isPlan: any;
  public iniciarSeguimiento = false;
  public id_plan_proyecto: any;
  public NombreProgramaDocente;
  public actividades: any;
  public departDate: Date;
  public returnDate: Date;
  public minTripDate = new Date();
  public maxTripDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
  public cargar_cambios = false;
  public cargar = false;
  //variable que guarda toda la informacion de peso de las actividades
  public peso;
  // Variable que contiene todos los seguimientos de un proyecto
  public lista_seguimientos: any;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private location: Location,
    private router: Router,
    private activeRouter: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.proyecto = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      objetivo: ['', [Validators.required]],
      programa: ['', [Validators.required]],
      selectPrograma: ['', [Validators.required]],
      NombreProgramaDocente: [{ value: '', disabled: true }, [Validators.required]],
    });
    this.registroActividad = this.fb.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      indicador_id: ['', [Validators.required]],
      fecha_inicio: ['', [Validators.required]],
      fecha_fin: ['', [Validators.required]],
      costo: ['', [Validators.required]],
      unidad_medida: ['', [Validators.required]],
      peso: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
      recursos: ['', [Validators.required]],
    });
    this.formSeguimiento = this.fb.group({
      periodo: ['', [Validators.required]],
    });
    this.registroResponsable = this.fb.group({
      responsables: ['', [Validators.required]],
    });
    this.obtenerCampos();
    this.tabla();
    this.listarProgramas();
    this.listarRecusos();
    this.listarIndicadores();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }

  obtenerCampos() {
    this.activeRouter.queryParams.subscribe(params => {
      this.id = params['id'];
      this.isPlan = params['isPlan'];
      this.idPlan = params['idPlan'];
      this.isPlanFin = params['isPlanFin'];
      if (this.isPlanFin == 'true') {
        this.isPlanFin = true;
      } else {
        this.isPlanFin = false;
      }
      this.usuario = this.provider.obtenerUser();
      this.consultarProyecto();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  agregarProyecto() {
    //this.router.navigateByUrl('/proyectos/registrar-proyecto');
    if (
      (this.indicadores == undefined && this.recursos == undefined) ||
      (this.indicadores.length == 0 && this.recursos.length == 0)
    ) {
      this.registroFallido('No hay indicadores ni recursos registrados');
    } else if (this.indicadores == undefined || this.indicadores.length == 0) {
      this.registroFallido('No hay indicadores registrados');
    } else if (this.recursos == undefined || this.recursos.length == 0) {
      this.registroFallido('No hay recursos registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarActividarDialog(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.id,
        peso: this.peso,
        isPlan: this.isPlan,
        isFinalPlan: this.isPlanFin,
        IsResponsable: this.IsResponsable,
      },
    };
    this.router.navigate(['/actividades/modificar-actividad'], navigationExtras);
  }
  eliminarActividadDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  //Metodo que muestra el dialogo de registro de responsables
  agregarResponsable() {
    //this.router.navigateByUrl('/proyectos/registrar-proyecto');
    if (this.responsables == undefined || this.responsables.length == 0) {
      this.registroFallido('No hay usuarios registrados');
    } else {
      this.dialog.open(this.agregar_responsables);
    }
  }
  //Metodo que muestra el dialogo de eliminar de responsables
  eliminarResponsableDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar_responsables);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  consultarProyecto() {
    this.peso = 0;
    let url;
    if (this.isPlan == 'true') {
      url = '/api/plan/proyecto/' + this.id + '/' + this.idPlan;
    } else {
      url = '/api/proyecto/' + this.id;
    }

    this.provider.listar(url).subscribe(
      data => {
        this.proyecto.controls.nombre.setValue(data.data.nombre);
        this.proyecto.controls.NombreProgramaDocente.setValue(data.data.programa_academico.nombre);
        this.proyecto.controls.programa.setValue(data.data.programa_academico_id);
        this.proyecto.controls.objetivo.setValue(data.data.objetivo);
        this.proyecto.controls.descripcion.setValue(data.data.descripcion);
        this.registroResponsable.controls.responsables.setValue(data.data.responsables);
        this.responsables_seleccionados = data.data.responsables;
        let usuario_id = this.usuario.id;
        let IsResponsable = false;
        this.responsables_seleccionados.forEach(function(element) {
          if (usuario_id == element.usuario.id) {
            IsResponsable = true;
          }
        });

        this.IsResponsable = IsResponsable;

        //
        // Este trozo de codigo me validaba si un proyecto ya estaba en seguimiento
        //
        //if (data.data.planes_proyectos[0] != undefined) {
        //  this.id_plan_proyecto = data.data.planes_proyectos[0].id;
        //  this.listarPeridos();
        //  this.iniciarSeguimiento = true;
        //}

        let programasSeleccinado = [];
        data.data.programas.forEach(function(element) {
          programasSeleccinado.push(element.programa_id);
        });
        this.proyecto.patchValue({ selectPrograma: programasSeleccinado });
        this.actividades = data.data.actividades;
        let pesos = 0;
        if (this.actividades.length > 0) {
          this.actividades.forEach(function(element) {
            pesos += element.peso;
          });
          this.peso = pesos;
        }
        this.listarResponsable();
        this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
          dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            if (index == 0) {
              // Call the dtTrigger to rerender again
              this.dtTrigger.next();
            }
          });
        });
      },
      error => {}
    );
  }
  /*
  
  METODOS UTILIZADO PARA EL MANEJO DE RESPONSABLES

  */

  listarResponsable() {
    let url = '/api/rol/3';
    this.provider.listar(url).subscribe(
      data => {
        this.responsables = data.data;
      },
      error => {}
    );
  }
  registrar_responsable() {
    if (this.registroResponsable.valid) {
      this.cargar = true;
      let data = {
        proyecto_id: this.id,
        responsables: this.registroResponsable.controls.responsables.value,
      };
      let url = '/api/proyecto/responsable';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Asignación exitosa!!');
            this.consultarProyecto();
            this.registroResponsable.reset();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Asignación fallida!!');
        }
      );
    }
  }
  eliminar_responsable() {
    this.cargar = true;
    let url = '/api/proyecto/responsable/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso(data.message);
          this.consultarProyecto();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  /*
  /
  /
  */
  listarProgramas() {
    let url = '/api/programa';
    this.provider.listar(url).subscribe(
      data => {
        this.programas = data.data;
      },
      error => {}
    );
  }

  listarRecusos() {
    let url = '/api/recurso';
    this.provider.listar(url).subscribe(
      data => {
        this.recursos = data.data;
      },
      error => {}
    );
  }

  listarIndicadores() {
    let url = '/api/indicador';
    this.provider.listar(url).subscribe(
      data => {
        this.indicadores = data.data;
      },
      error => {}
    );
  }

  listarPeridos() {
    this.periodos = [];
    let url = '/api/plan/proyecto/seguimiento/periodos/' + this.id_plan_proyecto;
    this.provider.listar(url).subscribe(
      data => {
        function compare(a, b) {
          // Use toUpperCase() to ignore character casing
          const bandA = a.periodo;
          const bandB = b.periodo;

          let comparison = 0;
          if (bandA > bandB) {
            comparison = 1;
          } else if (bandA < bandB) {
            comparison = -1;
          }
          return comparison;
        }
        function compare2(a, b) {
          // Use toUpperCase() to ignore character casing
          const bandA = a.periodo;
          const bandB = b.periodo;

          let comparison = 0;
          if (bandA < bandB) {
            comparison = 1;
          } else if (bandA > bandB) {
            comparison = -1;
          }
          return comparison;
        }
        this.lista_seguimientos = Object.values(data.data).sort(compare2);
        this.periodos = Object.values(data.data).sort(compare);
        let bandera = false;
        let aux = [];
        this.periodos.forEach(function(element) {
          if (!bandera) {
            if (element.fecha_seguimiento == '') {
              bandera = true;
              aux.push(element);
            }
          }
        });
        this.periodos = aux;
      },
      error => {}
    );
  }

  modificar_Proyecto() {
    if (this.proyecto.valid) {
      this.cargar = true;
      let data = {
        nombre: this.proyecto.controls.nombre.value,
        descripcion: this.proyecto.controls.descripcion.value,
        objetivo: this.proyecto.controls.objetivo.value,
        programa_academico_id: this.proyecto.controls.programa.value,
        programas: this.proyecto.controls.selectPrograma.value,
      };
      let url = '/api/proyecto/' + this.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.cargar = false;
            this.registroExitoso('Guardado exitoso!!');
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Guardado fallido!!');
        }
      );
    }
  }

  registrar_actividad(item: any) {
    if (this.registroActividad.valid) {
      if (this.registroActividad.controls.peso.value + this.peso > 100) {
        this.registroFallido('La suma total del peso excede el valor de 100');
        return;
      }
      this.cargar = true;
      var isoString = this.registroActividad.controls.fecha_inicio.value.toUTCString();
      var utc = new Date(isoString).toUTCString();
      var isoString2 = this.registroActividad.controls.fecha_fin.value.toUTCString();
      var utc2 = new Date(isoString2).toUTCString();
      let data = {
        proyecto_id: this.id,
        actividades: [
          {
            indicador_id: this.registroActividad.controls.indicador_id.value,
            nombre: this.registroActividad.controls.nombre.value,
            descripcion: this.registroActividad.controls.descripcion.value,
            fecha_inicio:
              new Date(utc).getFullYear() +
              '-' +
              (new Date(utc).getMonth() + 1) +
              '-' +
              new Date(utc).getDate(),
            fecha_fin:
              new Date(utc2).getFullYear() +
              '-' +
              (new Date(utc2).getMonth() + 1) +
              '-' +
              new Date(utc2).getDate(),
            costo: this.registroActividad.controls.costo.value,
            unidad_medida: this.registroActividad.controls.unidad_medida.value,
            peso: this.registroActividad.controls.peso.value,
            recursos: this.registroActividad.controls.recursos.value,
          },
        ],
      };
      let url = '/api/proyecto/actividad';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.consultarProyecto();
            this.registroActividad.reset();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  eliminarActividad() {
    this.cargar = true;
    let url = '/api/proyecto/actividad/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Actividad eliminada exitosamente!!');
          this.consultarProyecto();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  iniciar_seguimiento() {
    if (this.actividades.length == 0) {
      this.registroFallido('Por favor debes registrar actividades');
      return;
    }
    if (this.formSeguimiento.valid) {
      this.cargar = true;
      let data = {
        plan_proyecto_id: this.id_plan_proyecto,
        periodo_evaluado: this.formSeguimiento.controls.periodo.value,
      };
      let url = '/api/plan/proyecto/seguimiento/iniciar';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Inicio exitoso!!');
            this.consultarProyecto();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Inicio fallido!!');
        }
      );
    }
  }
  finalizar_seguimiento(item) {
    this.cargar = true;
    let data = {
      proyecto_plan_id: this.id_plan_proyecto,
      periodo: item.periodo,
    };
    let url = '/api/plan/proyecto/seguimiento/terminar';
    this.provider.registrar(url, data).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Finalización exitosa!!');
          this.listarPeridos();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Finalización fallida!!');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }

  volver(event) {
    if (event == 0) {
      this.location.back();
    } else if (event == 1) {
      this.titulo = 'Modificar Proyecto';
    } else if (event == 2) {
      this.titulo = 'Actividades Asignadas Al Proyecto';
    }
  }
}
