import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecursosListarRecursoComponent } from './listar-recurso/listar-recurso.component';

const routes: Routes = [{ path: '', component: RecursosListarRecursoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecursosRoutingModule { }
