import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { RecursosRoutingModule } from './recursos-routing.module';
import { RecursosListarRecursoComponent } from './listar-recurso/listar-recurso.component';
import { DataTablesModule } from 'angular-datatables';
const COMPONENTS = [RecursosListarRecursoComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    RecursosRoutingModule,
    DataTablesModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_DYNAMIC
  ],
  entryComponents: COMPONENTS_DYNAMIC
})
export class RecursosModule { }
