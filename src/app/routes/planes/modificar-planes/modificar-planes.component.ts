import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { MatDatepicker } from '@angular/material';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-planes-modificar-planes',
  templateUrl: './modificar-planes.component.html',
  styleUrls: ['./modificar-planes.component.scss'],
})
export class PlanesModificarPlanesComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar_proyecto', { static: false }) agregar_proyecto: TemplateRef<any>;
  @ViewChild('eliminar_proyecto_Dialog', { static: false }) eliminar_proyecto_Dialog: TemplateRef<
    any
  >;
  @ViewChild('fecha_inicio', { static: false }) fecha_inicio: MatDatepicker<any>;
  @ViewChild('fecha_fin', { static: false }) fecha_fin: MatDatepicker<any>;

  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public modificarPlan: FormGroup;
  public agregarProyectos: FormGroup;
  public proyectos: any;
  public usuario;
  public listaProyectos: any;
  public titulo = 'Modifica Plan';
  public programaAcademicos: any;
  public nombre_archivo;
  public invalidDate = false;
  public item: any;
  public id;
  public isPlanFin = false;
  public departDate: Date;
  public returnDate: Date;
  public minTripDate = new Date();
  public maxTripDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
  public cargar = false;

  urlBase;
  archivo;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private location: Location,
    private activeRouter: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.modificarPlan = this.fb.group({
      periodoInicio: [{ value: '', disabled: true }],
      periodoFin: [{ value: '', disabled: true }],
      fecha_Inicio: [{ value: '', disabled: true }, [Validators.required]],
      fecha_Fin: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      prograAcademico: ['', [Validators.required]],
      profile: [''],
      nombreArchivo: [{ value: '', disabled: true }, [Validators.required]],
    });
    this.agregarProyectos = this.fb.group({
      proyectos: ['', [Validators.required]],
    });
    this.obtenerCampos();
    this.listarProgramasAcademicos();
    this.listarProyectos();
    this.tabla();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(fX_ iltrado de _MAtotal entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }

  obtenerCampos() {
    this.activeRouter.queryParams.subscribe(params => {
      this.id = params['id'];
      this.usuario = this.provider.obtenerUser();
      this.consultarPlan();
    });

    this.urlBase = this.provider.obtenerUrl();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  agregarProtecto() {
    if (this.listaProyectos == undefined || this.listaProyectos.length == 0) {
      this.registroFallido('No hay proyectos disponibles');
    } else {
      this.dialog.open(this.agregar_proyecto);
    }
  }
  abrirReporte(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        proyecto: item.id,
        plan: this.id,
      },
    };
    this.router.navigate(['/reportes/proyecto-general-reporte'], navigationExtras);
  }

  eliminarProyectoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar_proyecto_Dialog);
  }

  modificarProyectoDialog(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.id,
        idPlan: this.id,
        isPlan: true,
        isPlanFin: this.isPlanFin,
      },
    };
    this.router.navigate(['/proyectos/modificar-proyecto'], navigationExtras);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  consultarPlan() {
    let url = '/api/plan/' + this.id;
    this.provider.listar(url).subscribe(
      data => {
        this.modificarPlan.controls.nombre.setValue(data.data.nombre);
        this.modificarPlan.controls.prograAcademico.setValue(data.data.programa_academico.id);
        this.modificarPlan.controls.nombreArchivo.setValue(data.data.url_documento.split('/')[1]);
        ////////////////
        this.modificarPlan.controls.fecha_Inicio.setValue(data.data.periodo_inicio);
        this.modificarPlan.controls.fecha_Fin.setValue(data.data.periodo_fin);
        ///////////////
        this.archivo = data.data.url_documento;
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.proyectos = data.data.proyectos;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });

        if (data.data.fecha_cierre == null) {
          this.isPlanFin = true;
        }
      },
      error => {}
    );
  }

  listarProyectos() {
    let usuario: any = this.provider.obtenerUser();
    let url = '/api/proyecto/getProgramaAcademico/' + usuario.programa_academico_id;
    this.provider.listar(url).subscribe(
      data => {
        this.listaProyectos = data.data;
      },
      error => {}
    );
  }

  listarProgramasAcademicos() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.modificarPlan.get('profile').setValue(file);
      this.modificarPlan.controls.nombreArchivo.setValue(
        this.modificarPlan.get('profile').value.name
      );
    }
  }

  modificar_plan(finalizar) {
    if (this.modificarPlan.valid) {
      if (
        this.modificarPlan.controls.fecha_Inicio.value > this.modificarPlan.controls.fecha_Fin.value
      ) {
        this.invalidDate = true;
        return;
      }
      this.cargar = true;
      const formData = new FormData();
      if (this.modificarPlan.get('profile').value != '') {
        formData.append('documento', this.modificarPlan.get('profile').value);
      }
      formData.append('nombre', this.modificarPlan.controls.nombre.value);
      if (finalizar) {
        let utc = new Date().toUTCString();
        formData.append(
          'fecha_cierre',
          new Date(utc).getFullYear() +
            '-' +
            (new Date(utc).getMonth() + 1) +
            '-' +
            new Date(utc).getDate()
        );
        this.isPlanFin = false;
      } else {
        formData.append('fecha_cierre', null);
      }

      formData.append('periodo_inicio', this.modificarPlan.controls.fecha_Inicio.value);
      formData.append('periodo_fin', this.modificarPlan.controls.fecha_Fin.value);
      formData.append('programa_academico_id', this.modificarPlan.controls.prograAcademico.value);
      let url = '/api/plan/' + this.id;
      this.provider.modifcarMultiParte(url, formData).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.consultarPlan();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  agregar_proyecto_plan(item: any) {
    if (this.agregarProyectos.valid) {
      this.cargar = true;
      let data = {
        plan_id: this.id,
        proyectos: this.agregarProyectos.controls.proyectos.value,
      };
      let url = '/api/plan/proyecto/asignar';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.consultarPlan();
            this.listarProyectos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  eliminar_proyecto() {
    this.cargar = true;
    let url = '/api/plan/proyecto/desasignar/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Actividad eliminada exitosamente!!');
          this.consultarPlan();
          this.listarProyectos();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
  closeInicioPicker($event) {
    this.invalidDate = false;
    this.minTripDate = $event;
    var isoString = $event.toUTCString();
    var utc = new Date(isoString).toUTCString();
    this.modificarPlan.controls.periodoInicio.setValue($event);
    this.modificarPlan.controls.fecha_Inicio.setValue(new Date(utc).getFullYear());
    this.fecha_inicio.close();
  }
  closeFinPicker($event) {
    this.invalidDate = false;
    var isoString = $event.toUTCString();
    var utc = new Date(isoString).toUTCString();
    this.modificarPlan.controls.periodoFin.setValue($event);
    this.modificarPlan.controls.fecha_Fin.setValue(new Date(utc).getFullYear());
    this.fecha_fin.close();
  }

  volver(event) {
    if (event == 0) {
      this.location.back();
    } else if (event == 1) {
      this.titulo = 'Modificar Plan';
    } else if (event == 2) {
      this.titulo = 'Proyectos asignados';
    }
  }
}
