import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { PlanesRoutingModule } from './planes-routing.module';
import { PlanesListarPlanComponent } from './listar-plan/listar-plan.component';
import { DataTablesModule } from 'angular-datatables';
import { PlanesUploadFileUploadFileComponent } from './UploadFile/upload-file/upload-file.component';
import { PlanesModificarPlanesComponent } from './modificar-planes/modificar-planes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const COMPONENTS = [PlanesListarPlanComponent, PlanesModificarPlanesComponent];
const COMPONENTS_DYNAMIC = [PlanesUploadFileUploadFileComponent];

@NgModule({
  imports: [SharedModule, PlanesRoutingModule, DataTablesModule,FormsModule,
    ReactiveFormsModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class PlanesModule {}
