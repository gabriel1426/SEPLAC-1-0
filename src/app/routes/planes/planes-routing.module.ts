import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanesListarPlanComponent } from './listar-plan/listar-plan.component';
import { PlanesModificarPlanesComponent } from './modificar-planes/modificar-planes.component';

const routes: Routes = [{ path: '', component: PlanesListarPlanComponent }, { path: 'modificar-planes', component: PlanesModificarPlanesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanesRoutingModule {}
