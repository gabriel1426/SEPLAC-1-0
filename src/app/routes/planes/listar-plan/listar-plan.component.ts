import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { MatDatepicker } from '@angular/material';

@Component({
  selector: 'app-planes-listar-plan',
  templateUrl: './listar-plan.component.html',
  styleUrls: ['./listar-plan.component.scss'],
})
export class PlanesListarPlanComponent {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild('fecha_inicio', { static: false }) fecha_inicio: MatDatepicker<any>;
  @ViewChild('fecha_fin', { static: false }) fecha_fin: MatDatepicker<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;
  public minTripDate = new Date();
  public maxTripDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
  public startTripDate = new Date(Date.now() + 31 * 24 * 60 * 60 * 1000);
  public planes: any;
  public usuario;
  public minDate: Date = new Date();
  public programaAcademicos: any;
  public item: any;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public cargar = false;
  public registrarPLan: FormGroup;
  public modificarPlan: FormGroup;
  public invalidDate = false;
  public NombrePrograma;
  public Admis_docentes = true;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog
  ) {
    let utc = new Date().toUTCString();
    this.registrarPLan = this.fb.group({
      periodoInicio: [{ value: '', disabled: true }],
      periodoFin: [{ value: '', disabled: true }],
      fecha_Inicio: [{ value: new Date(utc).getFullYear(), disabled: true }, [Validators.required]],
      fecha_Fin: [
        { value: new Date(utc).getFullYear() + 3, disabled: false },
        [Validators.required],
      ],
      nombre: ['', [Validators.required]],
      prograAcademico: ['', [Validators.required]],
      NombreProgramaDocente: [{ value: '', disabled: true }, [Validators.required]],
      profile: [''],
      nombreArchivo: [{ value: '', disabled: true }, [Validators.required]],
    });
    this.usuario = this.provider.obtenerUser();
    this.tabla();
    this.consultarProgramaAcademico();
    this.listarPlanes();
    this.listarProgramaAcademico();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      autoWidth: true,
      pagingType: 'full_numbers',
      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarPlan() {
    this.dialog.open(this.agregar);
  }

  modificarPlanDialog(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.id,
      },
    };
    this.router.navigate(['/planes/modificar-planes'], navigationExtras);
  }
  abrirReporte(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: item.id,
        fecha_inicio: item.periodo_inicio,
        fecha_fin: item.periodo_fin,
      },
    };
    this.router.navigate(['/reportes/plan-general-reporte'], navigationExtras);
  }

  EliminarplanDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarPlanes() {
    let url;
    if (this.usuario.rol_id == 1 || this.usuario.rol_id == 2) {
      url = '/api/plan';
    } else {
      url = '/api/programa_academico/' + this.usuario.programa_academico_id;
    }

    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          if (this.usuario.rol_id == 1 || this.usuario.rol_id == 2) {
            (this.Admis_docentes = true), (this.planes = data.data);
          } else {
            (this.Admis_docentes = false), (this.NombrePrograma = data.data.nombre);
            this.planes = data.data.planes;
          }
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }
  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }
  consultarProgramaAcademico() {
    let usuario: any = this.provider.obtenerUser();
    if (usuario.rol_id == 3 || usuario.rol_id == 4) {
      let url = '/api/programa_academico/' + usuario.programa_academico_id;
      this.provider.listar(url).subscribe(
        data => {
          this.registrarPLan.controls.prograAcademico.setValue(usuario.programa_academico_id);
          this.registrarPLan.controls.NombreProgramaDocente.setValue(data.data.nombre);
        },
        error => {}
      );
    }
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.registrarPLan.get('profile').setValue(file);
      this.registrarPLan.controls.nombreArchivo.setValue(
        this.registrarPLan.get('profile').value.name
      );
    }
  }

  registrar_plan() {
    if (this.registrarPLan.valid) {
      if (
        this.registrarPLan.controls.fecha_Inicio.value >=
        this.registrarPLan.controls.fecha_Fin.value
      ) {
        this.invalidDate = true;
        return;
      }
      this.invalidDate = false;
      this.cargar = true;
      const formData = new FormData();
      formData.append('documento', this.registrarPLan.get('profile').value);
      formData.append('nombre', this.registrarPLan.controls.nombre.value);
      formData.append('periodo_inicio', this.registrarPLan.controls.fecha_Inicio.value);
      formData.append('periodo_fin', this.registrarPLan.controls.fecha_Fin.value);
      formData.append('programa_academico_id', this.registrarPLan.controls.prograAcademico.value);
      let url = '/api/plan';
      this.provider.registrarMultiParte(url, formData).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarPLan.reset();
            this.listarPlanes();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  eliminar_plan() {
    this.cargar = true;
    let url = '/api/plan/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Plan eliminada exitosamente!!');
          this.listarPlanes();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
  closeInicioPicker($event) {
    this.invalidDate = false;
    this.minTripDate = $event;
    var isoString = $event.toUTCString();
    var utc = new Date(isoString).toUTCString();
    this.registrarPLan.controls.periodoInicio.setValue($event);
    this.registrarPLan.controls.fecha_Inicio.setValue(new Date(utc).getFullYear());
    this.fecha_inicio.close();
  }
  closeFinPicker($event) {
    this.invalidDate = false;
    var isoString = $event.toUTCString();
    var utc = new Date(isoString).toUTCString();
    this.registrarPLan.controls.periodoFin.setValue($event);
    this.registrarPLan.controls.fecha_Fin.setValue(new Date(utc).getFullYear());
    this.fecha_fin.close();
  }
}
