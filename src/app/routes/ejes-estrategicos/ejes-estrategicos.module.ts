import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { EjesEstrategicosRoutingModule } from './ejes-estrategicos-routing.module';
import { EjesEstrategicosListarEjeComponent } from './listar-eje/listar-eje.component';
import { DataTablesModule } from 'angular-datatables';
const COMPONENTS = [EjesEstrategicosListarEjeComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, EjesEstrategicosRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class EjesEstrategicosModule {}
