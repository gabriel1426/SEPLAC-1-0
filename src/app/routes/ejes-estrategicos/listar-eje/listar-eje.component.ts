import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-ejes-estrategicos-listar-eje',
  templateUrl: './listar-eje.component.html',
  styleUrls: ['./listar-eje.component.scss'],
})
export class EjesEstrategicosListarEjeComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  dtTrigger: any = new Subject();
  public item: any;
  public ejes: any;
  registrarEjes: FormGroup;
  modificarEjes: FormGroup;
  public cargar = false;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.listarEjes();
    this.registrarEjes = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });

    this.modificarEjes = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: [{ value: '', disabled: false }, [Validators.required]],
    });
    this.tabla();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarEje() {
    this.dialog.open(this.agregar);
  }

  modificarEjeDialog(item: any) {
    this.item = item;
    this.modificarEjes.controls.codigo.setValue(item.codigo);
    this.modificarEjes.controls.nombre.setValue(item.nombre);
    this.modificarEjes.controls.descripcion.setValue(item.descripcion);
    this.dialog.open(this.modificar);
  }

  EliminarEjeDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarEjes() {
    let url = '/api/eje';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.ejes = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisrtarEje() {
    if (this.registrarEjes.valid) {
      this.cargar = true;
      let data = {
        ejes: [
          {
            codigo: this.registrarEjes.controls.codigo.value,
            nombre: this.registrarEjes.controls.nombre.value,
            descripcion: this.registrarEjes.controls.descripcion.value,
          },
        ],
      };
      let url = '/api/eje';

      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarEjes.reset();
            this.listarEjes();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modificarEje(item: any) {
    if (this.modificarEjes.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarEjes.controls.codigo.value,
        nombre: this.modificarEjes.controls.nombre.value,
        descripcion: this.modificarEjes.controls.descripcion.value,
      };
      let url = '/api/eje/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarEjes();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarEje() {
    this.cargar = true;
    let url = '/api/eje/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Eje eliminado exitosamente!!');
          this.listarEjes();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
