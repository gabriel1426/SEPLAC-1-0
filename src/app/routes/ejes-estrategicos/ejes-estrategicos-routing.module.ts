import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EjesEstrategicosListarEjeComponent } from './listar-eje/listar-eje.component';

const routes: Routes = [{ path: '', component: EjesEstrategicosListarEjeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EjesEstrategicosRoutingModule {}
