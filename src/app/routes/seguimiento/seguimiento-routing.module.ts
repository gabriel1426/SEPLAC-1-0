import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeguimientoActividadesComponent } from './actividades/actividades.component';

const routes: Routes = [{ path: '', component: SeguimientoActividadesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeguimientoRoutingModule {}
