import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { SeguimientoRoutingModule } from './seguimiento-routing.module';
import { SeguimientoActividadesComponent } from './actividades/actividades.component';
import { DataTablesModule } from 'angular-datatables';
const COMPONENTS = [SeguimientoActividadesComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, SeguimientoRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class SeguimientoModule {}
