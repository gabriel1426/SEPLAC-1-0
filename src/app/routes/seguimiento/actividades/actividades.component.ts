import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-seguimiento-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.scss'],
})
export class SeguimientoActividadesComponent implements AfterViewInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;
  public usuario;
  public idPlan;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public actividades: any;
  constructor(private provider: ServicesAppService, private router: Router) {
    this.usuario = this.provider.obtenerUser();
    this.tabla();
    this.consultarActividades();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  consultarActividades() {
    let url = '/api/proyecto/responsable/' + this.usuario.id;
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          if (data.status != 'error') {
            this.idPlan = data.data[0].plan.id;
            this.actividades = data.data[0].proyectos;
          }

          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }
  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  verSeguimiento(actividad) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        id: actividad.id,
        idPlan: this.idPlan,
        isPlan: true,
      },
    };
    this.router.navigate(['/proyectos/modificar-proyecto'], navigationExtras);
  }
}
