import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndicadoresListarIndicadorComponent } from './listar-indicador/listar-indicador.component';

const routes: Routes = [{ path: '', component: IndicadoresListarIndicadorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndicadoresRoutingModule { }
