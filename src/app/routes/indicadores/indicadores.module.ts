import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { IndicadoresRoutingModule } from './indicadores-routing.module';
import { IndicadoresListarIndicadorComponent } from './listar-indicador/listar-indicador.component';
import { DataTablesModule } from 'angular-datatables';
const COMPONENTS = [IndicadoresListarIndicadorComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    DataTablesModule,
    IndicadoresRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_DYNAMIC
  ],
  entryComponents: COMPONENTS_DYNAMIC
})
export class IndicadoresModule { }
