import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-indicadores-listar-indicador',
  templateUrl: './listar-indicador.component.html',
  styleUrls: ['./listar-indicador.component.scss'],
})
export class IndicadoresListarIndicadorComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public indicadores: any;
  public registrarIndicador: FormGroup;
  public modificarIndicador: FormGroup;
  public cargar = false;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarIndicador = this.fb.group({
      nombre: ['', [Validators.required]],
    });

    this.modificarIndicador = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
    });
    this.tabla();
    this.listarIndicador();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarIndicador() {
    this.dialog.open(this.agregar);
  }

  modificarIndicadorDialog(item: any) {
    this.item = item;
    this.modificarIndicador.controls.codigo.setValue(item.id);
    this.modificarIndicador.controls.nombre.setValue(item.nombre);

    this.dialog.open(this.modificar);
  }

  EliminarIndicadorDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarIndicador() {
    let url = '/api/indicador';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.indicadores = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisIndicador() {
    if (this.registrarIndicador.valid) {
      this.cargar = true;
      let data = {
        indicadores: [this.registrarIndicador.controls.nombre.value],
      };
      let url = '/api/indicador';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarIndicador.reset();
            this.listarIndicador();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiIndicador(item: any) {
    if (this.modificarIndicador.valid) {
      this.cargar = true;
      let data = {
        nombre: this.modificarIndicador.controls.nombre.value,
      };
      let url = '/api/indicador/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarIndicador();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarIndicador() {
    this.cargar = true;
    let url = '/api/indicador/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Indicador eliminado exitosamente!!');
          this.listarIndicador();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
