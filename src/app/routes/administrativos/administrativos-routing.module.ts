import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrativosListarAdministrativoComponent } from './listar-administrativo/listar-administrativo.component';

const routes: Routes = [{ path: '', component: AdministrativosListarAdministrativoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrativosRoutingModule {}
