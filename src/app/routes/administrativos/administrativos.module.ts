import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { AdministrativosRoutingModule } from './administrativos-routing.module';
import { AdministrativosListarAdministrativoComponent } from './listar-administrativo/listar-administrativo.component';
import { DataTablesModule } from 'angular-datatables';
const COMPONENTS = [AdministrativosListarAdministrativoComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, AdministrativosRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class AdministrativosModule {}
