import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-administrativos-listar-administrativo',
  templateUrl: './listar-administrativo.component.html',
  styleUrls: ['./listar-administrativo.component.scss'],
})
export class AdministrativosListarAdministrativoComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public administrativos: any;
  public registrarAdministrativo: FormGroup;
  public modificarAdministrativo: FormGroup;
  public cargar = false;
  public usuario;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarAdministrativo = this.fb.group({
      codigo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });

    this.modificarAdministrativo = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });
    this.tabla();
    this.usuario = this.provider.obtenerUser();
    this.listarAdministrativos();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  getErrorMessage(form: FormGroup) {
    return form.get('email').hasError('required')
      ? 'Por favor ingresa el correo'
      : form.get('email').hasError('email')
      ? 'Correo no valido'
      : '';
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarAdministrativo() {
    this.dialog.open(this.agregar);
  }

  modificarAdministrativoDialog(item: any) {
    this.item = item;
    this.modificarAdministrativo.controls.codigo.setValue(item.codigo);
    this.modificarAdministrativo.controls.email.setValue(item.email);
    this.modificarAdministrativo.controls.nombre.setValue(item.name);
    this.modificarAdministrativo.controls.apellido.setValue(item.apellidos);
    this.dialog.open(this.modificar);
  }

  EliminarAdministrativoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarAdministrativos() {
    let url = '/api/rol/2';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.administrativos = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisAdministrativo() {
    if (this.registrarAdministrativo.valid) {
      this.cargar = true;
      let data = {
        rol_id: 2,
        name: this.registrarAdministrativo.controls.nombre.value,
        apellidos: this.registrarAdministrativo.controls.apellido.value,
        codigo: this.registrarAdministrativo.controls.codigo.value,
        email: this.registrarAdministrativo.controls.email.value,
        contrato: null,
        programa_academico_id: null,
      };
      let url = '/api/users';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarAdministrativo.reset();
            this.listarAdministrativos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiAdministrativo() {
    if (this.modificarAdministrativo.valid) {
      this.cargar = true;
      let data = {
        rol_id: 2,
        name: this.modificarAdministrativo.controls.nombre.value,
        apellidos: this.modificarAdministrativo.controls.apellido.value,
        codigo: this.modificarAdministrativo.controls.codigo.value,
        email: this.modificarAdministrativo.controls.email.value,
        contrato: null,
        programa_academico_id: null,
      };
      let url = '/api/users/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarAdministrativos();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarAdministrativo() {
    this.cargar = true;
    let url = '/api/users/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Administrativo eliminado exitosamente!!');
          this.listarAdministrativos();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
