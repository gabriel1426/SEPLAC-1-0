import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reportes-plan-general-reporte',
  templateUrl: './plan-general-reporte.component.html',
  styleUrls: ['./plan-general-reporte.component.scss'],
})
export class ReportesPlanGeneralReporteComponent {
  public title = 'Reporte';
  private date = new Date();
  public plan;
  public inicio;
  public fin;
  public url;
  public token;
  public periodo;
  public periodosSeleccionar;
  public fecha =
    this.date.getDay() + '/' + (this.date.getMonth() + 1) + '/' + this.date.getUTCFullYear();

  public reporte: any;
  public ejes = [];
  public idEje = 'sinFiltro';
  constructor(
    private provider: ServicesAppService,
    private location: Location,
    private activeRouter: ActivatedRoute
  ) {
    this.obtenerId();
    this.url = this.provider.obtenerUrl();
    this.token = this.provider.obtenerToken();
  }
  obtenerId() {
    this.activeRouter.queryParams.subscribe(params => {
      this.plan = params['id'];
      this.inicio = params['fecha_inicio'];
      this.fin = params['fecha_fin'];
      this.consultarPeriodo(this.plan);
    });
  }

  consultarPeriodo(id_plan) {
    let crearPeriodos = [];
    this.periodo = parseInt(this.inicio);
    while (this.inicio != this.fin) {
      crearPeriodos.push(parseInt(this.inicio));
      this.inicio = parseInt(this.inicio) + 1;
    }
    crearPeriodos.push(this.inicio);
    this.periodosSeleccionar = crearPeriodos;
    this.llenarReporte();
  }

  llenarReporte() {
    let url;
    let data;
    if (this.idEje == 'sinFiltro') {
      url = '/api/reportes/resumenPlanPeriodoPrograma';
      data = {
        plan_id: this.plan,
        periodo: this.periodo,
      };
    } else {
      url = '/api/reportes/cargarReportePeriodoEje';
      data = {
        plan_id: this.plan,
        eje_id: this.idEje,
        periodo: this.periodo,
      };
    }
    let aux = [];
    this.provider.registrar(url, data).subscribe(
      data => {
        this.reporte = data.data;

        this.reporte.plan.proyectos.forEach(element => {
          aux.push({ id: element.eje_id, nombre: element.eje });
        });
        this.ejes = this.eliminarDuplicados(aux);
      },
      error => {}
    );
  }

  eliminarDuplicados(dato) {
    let sinRepetidosx = dato.filter((valorActual, indiceActual, arreglo) => {
      return (
        arreglo.findIndex(
          valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
      );
    });
    return sinRepetidosx;
  }

  volver() {
    this.location.back();
  }
}
