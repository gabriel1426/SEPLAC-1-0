import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { ReportesRoutingModule } from './reportes-routing.module';
import { ReportesListarReportesComponent } from './listar-reportes/listar-reportes.component';
import { DataTablesModule } from 'angular-datatables';
import { ReportesPlanGeneralReporteComponent } from './plan-general-reporte/plan-general-reporte.component';
import { ReportesProyectoGeneralReporteComponent } from './proyecto-general-reporte/proyecto-general-reporte.component';

const COMPONENTS = [ReportesListarReportesComponent, ReportesPlanGeneralReporteComponent, ReportesProyectoGeneralReporteComponent];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [SharedModule, ReportesRoutingModule, DataTablesModule],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class ReportesModule {}
