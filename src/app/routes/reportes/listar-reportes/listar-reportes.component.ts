import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-reportes-listar-reportes',
  templateUrl: './listar-reportes.component.html',
  styleUrls: ['./listar-reportes.component.scss'],
})
export class ReportesListarReportesComponent implements AfterViewInit, OnDestroy {
  
  constructor(
    
  ) {
    
  }
  ngAfterViewInit(): void {
   
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    
  }
}
