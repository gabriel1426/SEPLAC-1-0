import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportesListarReportesComponent } from './listar-reportes/listar-reportes.component';
import { ReportesPlanGeneralReporteComponent } from './plan-general-reporte/plan-general-reporte.component';
import { ReportesProyectoGeneralReporteComponent } from './proyecto-general-reporte/proyecto-general-reporte.component';

const routes: Routes = [{ path: '', component: ReportesListarReportesComponent }, { path: 'plan-general-reporte', component: ReportesPlanGeneralReporteComponent }, { path: 'proyecto-general-reporte', component: ReportesProyectoGeneralReporteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportesRoutingModule {}
