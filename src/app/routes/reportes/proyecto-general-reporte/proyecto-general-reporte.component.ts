import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reportes-proyecto-general-reporte',
  templateUrl: './proyecto-general-reporte.component.html',
  styleUrls: ['./proyecto-general-reporte.component.scss'],
})
export class ReportesProyectoGeneralReporteComponent {
  public title = 'Reporte';
  private date = new Date();
  public proyecto;
  public plan;
  public periodo;
  public url;
  public token;
  public periodosSeleccionar;
  public fecha =
    this.date.getDay() + '/' + (this.date.getMonth() + 1) + '/' + this.date.getUTCFullYear();

  public reporte: any;
  public mostrar = false;

  constructor(
    private provider: ServicesAppService,
    private location: Location,
    private activeRouter: ActivatedRoute
  ) {
    this.url = this.provider.obtenerUrl();
    this.token = this.provider.obtenerToken();
    this.obtenerId();
  }
  obtenerId() {
    this.activeRouter.queryParams.subscribe(params => {
      this.proyecto = params['proyecto'];
      this.plan = params['plan'];
      this.llenarReporte();
    });
  }

  llenarReporte() {
    let url = '/api/reportes/cargarResumenGeneralProyecto';
    let data = {
      plan_id: this.plan,
      proyecto_id: this.proyecto,
    };
    this.provider.registrar(url, data).subscribe(
      data => {
        this.mostrar = true;
        this.reporte = data.data;
      },
      error => {}
    );
  }
  volver(event) {
    if (event == 0) {
      this.location.back();
    }
  }
}
