import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import {
  Validators,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  FormsModule,
} from '@angular/forms';
import { ViewChild, ViewChildren, TemplateRef, QueryList } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { concat } from 'rxjs';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-actividades-modificar-actividad',
  templateUrl: './modificar-actividad.component.html',
  styleUrls: ['./modificar-actividad.component.scss'],
})
export class ActividadesModificarActividadComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar_recursos', { static: false }) agregar_recursos: TemplateRef<any>;
  @ViewChild('eliminar_recursos', { static: false }) eliminar_recursos: TemplateRef<any>;

  @ViewChild('agregar_comentario', { static: false }) agregar_comentario: TemplateRef<any>;

  @ViewChildren(DataTableDirective) dtElements: QueryList<any>;
  public Editor = ClassicEditor;
  public title = 'Modificar Actividad';
  public usuario;
  public urlBase;
  public selectedIndexTab = 1;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger_recursos: any = new Subject();
  public recursos_actividad: any;
  public modificarActividad: FormGroup;
  public registroRecurso: FormGroup;
  public indicadores: any;
  public recursosLista: any;
  // variable que nos indica si una actividad tiene seguimientos
  public isSeguimiento = false;
  // variable que nos ayuda a controlar los tabs para realizar seguimiento a las actividades
  public activarSeguimiento = false;
  // variable que nos ayuda a cambiar entre los tan de la actividad y los tab de los comentarios
  public isVerComentarios = false;
  // Variable que contendra los comentarios de una actividad
  public listaComentarios: any;
  // Variable que contiene todos los seguimientos de una actividad
  public lista_seguimientos: any;
  //Lista de los recursos de una actividad
  public recursos_seleccionados: any;
  //variable usada para saber si vengo de plan
  public isPlan: any;
  //variable usaba para saber si un plan ya termino
  public isPlanFin: any;
  //Variable usaba para saber si el usuario es responsable de la actividad
  public IsResponsable;
  public item: any;
  public item_comentario: any;
  // Editos
  public model = {
    editorData: '',
  };
  // Drop files
  public uploader: FileUploader = new FileUploader({});
  public hasBaseDropZoneOver: boolean = false;
  // Variable que me servira para iterar entre comentar o midificar comentario
  public isComentar = true;

  public id;
  public pesoTotal;
  public pesoActividad;
  public NombreProgramaDocente;
  public actividades: any;
  public departDate: Date;
  public returnDate: Date;
  public minTripDate = new Date();
  public maxTripDate = new Date(Date.now() + 365 * 24 * 60 * 60 * 1000);
  public cargar_seguimiento = false;
  public cargar = false;
  public proyecto;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private location: Location,
    private fb: FormBuilder,
    private router: Router,
    private activeRouter: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.obtenerCampos();
    if (this.isPlan == 'true') {
      this.modificarActividad = this.fb.group({
        nombre: [{ value: '', disabled: true }, [Validators.required]],
        descripcion: [{ value: '', disabled: true }, [Validators.required]],
        indicador_id: [{ value: '', disabled: true }, [Validators.required]],
        fecha_inicio: [{ value: '', disabled: true }, [Validators.required]],
        fecha_fin: [{ value: '', disabled: true }, [Validators.required]],
        costo: ['', [Validators.required]],
        estado: ['', [Validators.required]],
        unidad_medida: [{ value: '', disabled: true }, [Validators.required]],
        peso: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
      });
    } else {
      this.modificarActividad = this.fb.group({
        nombre: [{ value: '', disable: '' }, [Validators.required]],
        descripcion: ['', [Validators.required]],
        indicador_id: ['', [Validators.required]],
        fecha_inicio: ['', [Validators.required]],
        fecha_fin: ['', [Validators.required]],
        costo: ['', [Validators.required]],
        unidad_medida: ['', [Validators.required]],
        peso: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
      });
    }
    this.registroRecurso = this.fb.group({
      recursos: ['', [Validators.required]],
    });

    this.tabla();
    this.listarRecusos();
    this.listarIndicadores();
    this.consultarSeguimientos();
    this.urlBase = this.provider.obtenerUrl();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }

  obtenerCampos() {
    this.activeRouter.queryParams.subscribe(params => {
      this.id = params['id'];
      this.pesoTotal = params['peso'];
      this.isPlan = params['isPlan'];
      this.isPlanFin = params['isFinalPlan'];
      if (this.isPlanFin == 'true') {
        this.isPlanFin = true;
      } else {
        this.isPlanFin = false;
      }
      if (this.IsResponsable == 'true') {
        this.IsResponsable = true;
      } else {
        this.IsResponsable = false;
      }
      this.usuario = this.provider.obtenerUser();
      this.consultarActividad();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger_recursos.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger_recursos.unsubscribe();
  }

  /*
  
  METODOS ENCARGADO DE MANEJAR LOS DIALOGOS

  */
  agregarRecurso() {
    //this.router.navigateByUrl('/proyectos/registrar-proyecto');
    if (this.recursosLista == undefined || this.recursosLista.length == 0) {
      this.registroFallido('No hay recursos registrados');
    } else {
      this.dialog.open(this.agregar_recursos);
    }
  }

  agregarComentario() {
    this.isComentar = true;
    this.model.editorData = '';
    this.uploader = new FileUploader({});
    this.dialog.open(this.agregar_comentario);
  }

  eliminarRecursoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar_recursos);
  }

  /*
  
  METODOS UTILIZADO PARA CARGAR LOS COMBOS CON LA INFO DE LA ACTIVIDAD

  */
  listarIndicadores() {
    let url = '/api/indicador';
    this.provider.listar(url).subscribe(
      data => {
        this.indicadores = data.data;
      },
      error => {}
    );
  }

  /*
  
  METODOS UTILIZADO PARA EL MANEJO DE LA INFORMACION DE LA ACTIVIDAD

  */
  consultarActividad() {
    this.isVerComentarios = false;
    let url;
    if (this.isPlan == 'true') {
      url = '/api/plan/proyecto/actividad/ver/' + this.id;
    } else {
      url = '/api/proyecto/actividad/' + this.id;
    }

    this.provider.listar(url).subscribe(
      data => {
        if (this.isPlan == 'false') {
          this.proyecto = data.data.proyecto.nombre;
          this.modificarActividad.controls.nombre.setValue(data.data.nombre);
          this.modificarActividad.controls.descripcion.setValue(data.data.descripcion);
          this.modificarActividad.controls.costo.setValue(data.data.costo);
          var fecha_fin = new Date(data.data.fecha_fin);
          //nueva fecha sumada
          fecha_fin.setDate(fecha_fin.getDate() + 1);

          this.modificarActividad.controls.fecha_fin.setValue(fecha_fin);
          var fecha_inicio = new Date(data.data.fecha_inicio);
          //nueva fecha sumada
          fecha_inicio.setDate(fecha_inicio.getDate() + 1);
          this.minTripDate = fecha_inicio;
          this.modificarActividad.controls.fecha_inicio.setValue(fecha_inicio);
          this.modificarActividad.controls.indicador_id.setValue(data.data.indicador_id);

          this.modificarActividad.controls.peso.setValue(data.data.peso);
          this.pesoActividad = data.data.peso;
          this.modificarActividad.controls.unidad_medida.setValue(data.data.unidad_medida);

          this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.destroy();
              if (index == 0) {
                this.dtTrigger_recursos.next();
              }
            });
          });
          this.recursos_seleccionados = data.data.actividades_recursos;
        } else {
          this.id = data.data.id;
          this.proyecto = data.data.actividad.proyecto.nombre;
          this.modificarActividad.controls.nombre.setValue(data.data.actividad.nombre);
          this.modificarActividad.controls.descripcion.setValue(data.data.actividad.descripcion);
          this.modificarActividad.controls.costo.setValue(data.data.costo);
          var fecha_fin = new Date(data.data.fecha_fin);
          //nueva fecha sumada
          fecha_fin.setDate(fecha_fin.getDate() + 1);

          this.modificarActividad.controls.fecha_fin.setValue(fecha_fin);
          var fecha_inicio = new Date(data.data.fecha_inicio);
          //nueva fecha sumada
          fecha_inicio.setDate(fecha_inicio.getDate() + 1);
          this.minTripDate = fecha_inicio;
          this.modificarActividad.controls.fecha_inicio.setValue(fecha_inicio);
          this.modificarActividad.controls.indicador_id.setValue(data.data.actividad.indicador_id);

          this.modificarActividad.controls.peso.setValue(data.data.peso);
          this.pesoActividad = data.data.peso;
          this.modificarActividad.controls.unidad_medida.setValue(
            data.data.actividad.unidad_medida
          );

          this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              dtInstance.destroy();
              if (index == 0) {
                this.dtTrigger_recursos.next();
              }
            });
          });
          this.recursos_seleccionados = data.data.actividades_recursos;
        }
      },
      error => {}
    );
  }

  modificar_actividad() {
    if (this.modificarActividad.valid || this.isPlan == 'true') {
      if (this.pesoTotal - this.pesoActividad + this.modificarActividad.controls.peso.value > 100) {
        this.registroFallido('La suma total del peso excede el valor de 100');
        return;
      }
      this.cargar = true;
      var fecha_inicio;
      var fecha_fin;
      try {
        var isoString = this.modificarActividad.controls.fecha_inicio.value.toUTCString();
        var utc = new Date(isoString).toUTCString();
        fecha_inicio =
          new Date(utc).getFullYear() +
          '-' +
          (new Date(utc).getMonth() + 1) +
          '-' +
          new Date(utc).getDate();
      } catch (e) {
        fecha_inicio = this.modificarActividad.controls.fecha_inicio.value;
      }
      try {
        var isoString2 = this.modificarActividad.controls.fecha_fin.value.toUTCString();
        var utc2 = new Date(isoString2).toUTCString();
        fecha_fin =
          new Date(utc2).getFullYear() +
          '-' +
          (new Date(utc2).getMonth() + 1) +
          '-' +
          new Date(utc2).getDate();
      } catch (e) {
        fecha_fin = this.modificarActividad.controls.fecha_fin.value;
      }
      let data: any = {
        indicador_id: this.modificarActividad.controls.indicador_id.value,
        nombre: this.modificarActividad.controls.nombre.value,
        descripcion: this.modificarActividad.controls.descripcion.value,
        fecha_inicio: fecha_inicio,
        fecha_fin: fecha_fin,
        costo: this.modificarActividad.controls.costo.value,
        unidad_medida: this.modificarActividad.controls.unidad_medida.value,
        peso: this.modificarActividad.controls.peso.value,
      };
      let url;
      if (this.isPlan == 'true') {
        url = '/api/plan/proyecto/actividad/' + this.id;
        data = {
          estado: this.modificarActividad.controls.estado.value,
          costo: this.modificarActividad.controls.costo.value,
          peso: this.modificarActividad.controls.peso.value,
        };
      } else {
        url = '/api/proyecto/actividad/' + this.id;
      }

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.consultarActividad();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificación fallida!!');
        }
      );
    }
  }
  /*
  
  METODOS UTILIZADO PARA EL MANEJO DE RECURSOS

  */

  listarRecusos() {
    let url = '/api/recurso';
    this.provider.listar(url).subscribe(
      data => {
        this.recursosLista = data.data;
      },
      error => {}
    );
  }
  registrar_recurso() {
    if (this.registroRecurso.valid) {
      this.cargar = true;
      let data = {
        actividad_id: this.id,
        recursos: this.registroRecurso.controls.recursos.value,
      };
      let url = '/api/proyecto/actividad/recurso';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Asignación exitosa!!');
            this.consultarActividad();
            this.registroRecurso.reset();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Asignación fallida!!');
        }
      );
    }
  }

  eliminar_recurso() {
    this.cargar = true;
    let url = '/api/proyecto/actividad/recurso/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso(data.message);
          this.consultarActividad();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  /*
  
  METODOS UTILIZADO PARA EL MANEJO VALORACION Y SEGUIMIENTOS A 
  LAS ACTIVIDADES

  */
  consultarSeguimientos() {
    let url;
    if (this.usuario.rol_id == 3) {
      url = '/api/plan/proyecto/seguimiento/actividad/' + this.id;
    } else {
      url = '/api/plan/proyecto/seguimiento/actividad/anteriores/' + this.id;
    }

    this.provider.listar(url).subscribe(
      data => {
        function compare(a, b) {
          // Use toUpperCase() to ignore character casing
          const bandA = a.periodo_evaluado.toUpperCase();
          const bandB = b.periodo_evaluado.toUpperCase();

          let comparison = 0;
          if (bandA < bandB) {
            comparison = 1;
          } else if (bandA > bandB) {
            comparison = -1;
          }
          return comparison;
        }
        this.lista_seguimientos = Object.values(data.data);
        this.isSeguimiento = true;
      },
      error => {}
    );
  }

  registrar_seguimiento_actividad(item, finalizar: boolean) {
    this.cargar_seguimiento = true;
    var fecha;
    if (finalizar) {
      fecha =
        new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    } else {
      fecha = null;
    }
    let situacion_actual = '';
    if (item.valoracion > 0 && item.valoracion <= 39) {
      situacion_actual = 'Bajo';
    } else if (item.valoracion >= 40 && item.valoracion <= 74) {
      situacion_actual = 'Medio';
    } else {
      situacion_actual = 'Alto ';
    }
    let data = {
      actividad_id: item.plan_actividad_id,
      periodo_evaluado: item.periodo_evaluado,
      fecha_seguimiento: fecha,
      valoracion: item.valoracion,
      situacion_actual: situacion_actual,
    };
    let url = '/api/plan/proyecto/seguimiento/' + item.id;
    if (finalizar) {
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Finalización exitosa!!');
            this.consultarSeguimientos();
            this.dialog.closeAll();
            this.cargar_seguimiento = false;
          } else {
            this.cargar_seguimiento = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar_seguimiento = false;
          this.registroFallido('Finalización fallida!!');
        }
      );
    } else {
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Valoración exitosa!!');
            this.consultarSeguimientos();
            this.dialog.closeAll();
            this.cargar_seguimiento = false;
          } else {
            this.cargar_seguimiento = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar_seguimiento = false;
          this.registroFallido('Valoración fallida!!');
        }
      );
    }
  }

  /*
  
  METODOS UTILIZADO PARA EL MANEJO DE COMENTARIOS

  */
  consultar_comentarios_seguimiento() {
    let url = '/api/plan/proyecto/seguimiento/comentario/' + this.item.id;
    this.provider.listar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.listaComentarios = data.data;
        } else {
          this.listaComentarios = [];
        }
      },
      error => {
        this.listaComentarios = [];
        this.registroFallido('Registro fallido!!');
      }
    );
  }
  registrar_comentario() {
    this.cargar = true;
    let user: any = this.provider.obtenerUser();
    let data = {
      seguimiento_id: this.item.id,
      autor_id: user.id,
      comentario: this.model.editorData,
    };
    let url = '/api/plan/proyecto/seguimiento/comentario';
    this.provider.registrar(url, data).subscribe(
      data => {
        if (data.status == 'ok') {
          this.upload_files_evidencias(data.data);
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Registro fallido!!');
      }
    );
  }

  upload_files_evidencias(comentario_id) {
    let files = this.getFiles();
    let requests = [];
    if (files.length == 0) {
      this.consultar_comentarios_seguimiento();
      this.registroExitoso('Registro exitoso!!');
      this.cargar = false;
      this.dialog.closeAll();
    } else {
      files.forEach(file => {
        let formData = new FormData();
        formData.append('evidencia', file.rawFile, file.name);
        formData.append('comentario_id', comentario_id);
        requests.push(this.provider.upload(formData));
      });

      concat(...requests).subscribe(
        res => {
          this.consultar_comentarios_seguimiento();
        },
        err => {}
      );
      this.registroExitoso('Registro exitoso!!');
      this.cargar = false;
      this.dialog.closeAll();
    }
  }

  modificar_comentario() {
    this.cargar = true;
    let data = {
      comentario: this.model.editorData,
    };
    let url = '/api/plan/proyecto/seguimiento/comentario/' + this.item_comentario.id;
    this.provider.modifcar(url, data).subscribe(
      data => {
        if (data.status == 'ok') {
          this.upload_files_evidencias(data.data);
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Registro fallido!!');
      }
    );
  }

  eliminar_comentario(comentario) {
    let url = '/api/plan/proyecto/seguimiento/comentario/' + comentario.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.consultar_comentarios_seguimiento();
        } else {
          this.registroFallido(data.message);
        }
      },
      error => {
        this.registroFallido('Registro fallido!!');
      }
    );
  }

  editar_comentario(comentario) {
    this.isComentar = false;
    this.item_comentario = comentario;
    this.model.editorData = comentario.observacion;
    this.uploader = new FileUploader({});
    this.dialog.open(this.agregar_comentario);
  }

  verComentarios(item) {
    this.item = item;
    this.listaComentarios = [];
    this.title = 'Comentarios';
    this.consultar_comentarios_seguimiento();
    this.isVerComentarios = !this.isVerComentarios;
    this.selectedIndexTab = 4;
  }

  eliminar_evidencia(evidencia) {
    let url = '/api/plan/proyecto/seguimiento/comentario/evidencia/' + evidencia;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.consultar_comentarios_seguimiento();
        } else {
          this.registroFallido(data.message);
        }
      },
      error => {
        this.registroFallido('Registro fallido!!');
      }
    );
  }

  /*
  
  UTILS

  */
  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }

  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  fileOverBase(event): void {
    this.hasBaseDropZoneOver = event;
  }

  getFiles(): FileLikeObject[] {
    return this.uploader.queue.map(fileItem => {
      return fileItem.file;
    });
  }

  getColor(valoracion) {
    if ((valoracion > 0 && valoracion <= 39) || valoracion == null) {
      return 'red';
    } else if (valoracion >= 40 && valoracion <= 74) {
      return 'orange';
    } else if (valoracion >= 75 && valoracion <= 100) {
      return 'green';
    } else {
      return 'red';
    }
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
  getNombre(item: string) {
    return item.split('/')[1];
  }
  validarValorSeguimiento(valor): void {
    if ((valor.valoracion > 0 && valor.valoracion <= 39) || valor.valoracion == null) {
      valor.situacion_actual = 'Bajo';
      this.cargar_seguimiento = false;
      //return false;
    } else if (valor.valoracion >= 40 && valor.valoracion <= 74) {
      valor.situacion_actual = 'Medio';
      this.cargar_seguimiento = false;
      //return false;
    } else if (valor.valoracion >= 75 && valor.valoracion <= 100) {
      valor.situacion_actual = 'Alto ';
      this.cargar_seguimiento = false;
      //return false;
    } else {
      this.cargar_seguimiento = true;
      //return true;
    }
  }
  volver(event) {
    if (event == 0 && this.isVerComentarios) {
      this.isVerComentarios = !this.isVerComentarios;
      this.title = 'Modificar Actividad';
      this.selectedIndexTab = 1;
    } else if (event == 1) {
      this.isVerComentarios = false;
      this.title = 'Modificar Actividad';
      this.selectedIndexTab = 1;
    } else if (event == 2) {
      this.title = 'Recursos';
      this.selectedIndexTab = 2;
    } else if (event == 0) {
      this.location.back();
    }
  }
}
