import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { ActividadesRoutingModule } from './actividades-routing.module';
import { ActividadesRegistrarActividadComponent } from './registrar-actividad/registrar-actividad.component';
import { ActividadesListarActividadComponent } from './listar-actividad/listar-actividad.component';
import { ActividadesModificarActividadComponent } from './modificar-actividad/modificar-actividad.component';
import { DataTablesModule } from 'angular-datatables';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { FileUploadModule } from 'ng2-file-upload';

const COMPONENTS = [
  ActividadesRegistrarActividadComponent,
  ActividadesListarActividadComponent,
  ActividadesModificarActividadComponent,
];
const COMPONENTS_DYNAMIC = [];

@NgModule({
  imports: [
    SharedModule,
    ActividadesRoutingModule,
    DataTablesModule,
    CKEditorModule,
    FileUploadModule,
  ],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: [COMPONENTS_DYNAMIC],
})
export class ActividadesModule {}
