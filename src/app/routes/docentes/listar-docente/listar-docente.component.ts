import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-docentes-listar-docente',
  templateUrl: './listar-docente.component.html',
  styleUrls: ['./listar-docente.component.scss'],
})
export class DocentesListarDocenteComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public programaAcademicos: any;
  public docentes: any;
  public cargar = false;
  public usuario;
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  registrarDocente: FormGroup;
  modificarDocente: FormGroup;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.dtOptions = {
      ajax: 'data/data.json',

      // Use this attribute to enable the responsive extension
      responsive: true,
    };
    this.registrarDocente = this.fb.group({
      codigo: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });

    this.modificarDocente = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: ['', [Validators.required]],
      contrato: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    });
    this.tabla();
    this.usuario = this.provider.obtenerUser();
    this.listarDocentes();
    this.listarProgramaAcademico();
  }

  getErrorMessage(form: FormGroup) {
    return form.get('email').hasError('required')
      ? 'Por favor ingresa el correo'
      : form.get('email').hasError('email')
      ? 'Correo no valido'
      : '';
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(fX_ iltrado de _MAtotal entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarDocente() {
    if (this.programaAcademicos == undefined || this.programaAcademicos.length == 0) {
      this.registroFallido('No hay programas academicos registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarDocenteDialog(item: any) {
    this.item = item;
    this.modificarDocente.controls.codigo.setValue(item.codigo);
    this.modificarDocente.controls.email.setValue(item.email);
    this.modificarDocente.controls.nombre.setValue(item.name);
    this.modificarDocente.controls.apellido.setValue(item.apellidos);
    this.modificarDocente.patchValue({ contrato: item.contrato });
    this.modificarDocente.patchValue({ programa_academico_id: item.programa_academico_id });
    this.dialog.open(this.modificar);
  }

  EliminarDocenteDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }
  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }

  listarDocentes() {
    let url = '/api/rol/3';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.docentes = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisDocente() {
    if (this.registrarDocente.valid) {
      this.cargar = true;
      let data = {
        rol_id: 3,
        name: this.registrarDocente.controls.nombre.value,
        apellidos: this.registrarDocente.controls.apellido.value,
        codigo: this.registrarDocente.controls.codigo.value,
        email: this.registrarDocente.controls.email.value,
        contrato: this.registrarDocente.controls.contrato.value,
        programa_academico_id: this.registrarDocente.controls.programa_academico_id.value,
      };
      let url = '/api/users';

      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarDocente.reset();
            this.listarDocentes();
            this.cargar = false;
            this.dialog.closeAll();
            this.cargar = false;
          } else {
            this.registroFallido(data.message);
            this.cargar = false;
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiDocente() {
    if (this.modificarDocente.valid) {
      this.cargar = true;
      let data = {
        rol_id: 3,
        name: this.modificarDocente.controls.nombre.value,
        apellidos: this.modificarDocente.controls.apellido.value,
        codigo: this.modificarDocente.controls.codigo.value,
        email: this.modificarDocente.controls.email.value,
        contrato: this.modificarDocente.controls.contrato.value,
        programa_academico_id: this.modificarDocente.controls.programa_academico_id.value,
      };
      let url = '/api/users/' + this.item.id;

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarDocentes();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarDocente() {
    this.cargar = true;
    let url = '/api/users/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Docente eliminado exitosamente!!');
          this.listarDocentes();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
