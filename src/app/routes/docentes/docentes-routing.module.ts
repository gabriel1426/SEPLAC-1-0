import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocentesListarDocenteComponent } from './listar-docente/listar-docente.component';

const routes: Routes = [
  { path: '', component: DocentesListarDocenteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocentesRoutingModule {}
