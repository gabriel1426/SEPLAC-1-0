import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-sessions-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss'],
})
export class SessionsPerfilComponent {
  public modificarPerfil: FormGroup;
  public actualizarContrasena: FormGroup;
  public cargar = false;
  public programaAcademicos: any;
  public usuario: any;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    this.modificarPerfil = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      nombre: ['', [Validators.required]],
      programa_academico_id: [{ value: '', disabled: true }, [Validators.required]],
      contrato: [{ value: '', disabled: true }, [Validators.required]],
      apellido: ['', [Validators.required]],
    });
    this.actualizarContrasena = this.fb.group({
      actual: ['', [Validators.required]],
      nueva: ['', [Validators.required]],
      confirmar: ['', [Validators.required]],
    });
    this.consultar_datos();
  }
  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  consultar_datos() {
    this.usuario = this.provider.obtenerUser();
    this.modificarPerfil.controls.codigo.setValue(this.usuario.codigo);
    this.modificarPerfil.controls.email.setValue(this.usuario.email);
    this.modificarPerfil.controls.nombre.setValue(this.usuario.name);
    this.modificarPerfil.controls.apellido.setValue(this.usuario.apellidos);
    this.modificarPerfil.patchValue({ contrato: this.usuario.contrato });
    this.modificarPerfil.patchValue({ programa_academico_id: this.usuario.programa_academico_id });
    if (this.usuario.rol_id != 1 || this.usuario.rol_id != 2) {
      this.listarProgramaAcademico();
    }
  }

  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.programaAcademicos = data.data;
      },
      error => {}
    );
  }

  modificar_perfil() {
    if (this.modificarPerfil.valid || (this.usuario.rol_id == 1 || this.usuario.rol_id == 2)) {
      this.cargar = true;
      let data = {
        rol_id: this.usuario.rol_id,
        name: this.modificarPerfil.controls.nombre.value,
        apellidos: this.modificarPerfil.controls.apellido.value,
        codigo: this.modificarPerfil.controls.codigo.value,
        email: this.modificarPerfil.controls.email.value,
        contrato: this.modificarPerfil.controls.contrato.value,
        programa_academico_id: this.modificarPerfil.controls.programa_academico_id.value,
      };
      let url = '/api/users/' + this.usuario.id;

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Actualización exitosa!!');
            this.consultarPefil();
            this.cargar = false;
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Actualización fallida!!');
        }
      );
    }
  }
  consultarPefil() {
    this.provider.consultarPerfil().subscribe(data => {
      if (data.status == 'ok') {
        localStorage.setItem('user', JSON.stringify(data.data));
        this.provider.obtenerUser();
      }
    });
  }

  actualizar_contrasena() {
    if (this.actualizarContrasena.valid) {
      this.cargar = true;
      if (
        this.actualizarContrasena.controls.nueva.value !=
        this.actualizarContrasena.controls.confirmar.value
      ) {
        this.registroFallido('Las contraseñas no coinciden');
        this.cargar = false;
        return;
      }
      let data = {
        current_password: this.actualizarContrasena.controls.actual.value,
        new_password: this.actualizarContrasena.controls.nueva.value,
        email: this.modificarPerfil.controls.email.value,
      };
      let url = '/api/password/' + this.usuario.id;

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Actualización exitosa!!');
            this.actualizarContrasena.reset();
            this.consultarPefil();
            this.cargar = false;
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Actualización fallida!!');
        }
      );
    }
  }
}
