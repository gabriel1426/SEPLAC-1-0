import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  @ViewChild(FormGroupDirective, { static: false }) formDirective: FormGroupDirective;
  reactiveForm: FormGroup;
  public credenciales: boolean = true;
  public iniciar: boolean = false;
  public modo: string = '1'; // 1 => Login, 2 => ResetPassword, 3 => CambioPassword

  public codigo: string = '';
  public email: string = '';
  public password: string = '';
  public newPassword: string = '';

  public tokenReset: string = '';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private routeActive: ActivatedRoute
  ) {
    this.reactiveForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      codigo: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    let user:any = this.provider.obtenerUser();
    if (user != null) {
      if (user.rol_id != 3) {
        this.router.navigateByUrl('/planes');
      } else {
        this.router.navigateByUrl('/seguimiento');
      }
    }
    let urlTree = this.router.parseUrl(this.router.url);
    let k = urlTree.queryParams['k'];

    if (k) {

      this.modo = '3';
      k = atob(k);
      k = k.split('?');

      this.tokenReset = k[1].split('=')[1];
      this.email = k[0].split('=')[1];
    }
  }

  onSubmit() {
    if (this.modo == '1') {
      this.login();
    } else if (this.modo == '2') {
      if (
        this.reactiveForm.controls.email.value == '' ||
        this.reactiveForm.controls.codigo.value == '' ||
        this.reactiveForm.controls.codigo.value == null ||
        this.reactiveForm.controls.codigo.value == null
      ) {
        return this.registroFallido('Debe completar todos los campos');
      }

      this.provider
        .solicitarCambioContrasenia({
          email: this.reactiveForm.controls.email.value,
          codigo: this.reactiveForm.controls.codigo.value,
        })
        .subscribe(res => {
          if (res.status == 'ok') return this.registroExitoso(res.message);
          else return this.registroFallido(res.message);
        });
    } else if (this.modo == '3') {
      if (
        this.reactiveForm.controls.newPassword.value == '' ||
        this.reactiveForm.controls.password.value == '' ||
        this.reactiveForm.controls.email.value == '' ||
        this.reactiveForm.controls.newPassword.value == null ||
        this.reactiveForm.controls.email.value == null ||
        this.reactiveForm.controls.password.value == null
      ) {
        return this.registroFallido('Debe completar todos los campos');
      }
      if (this.reactiveForm.controls.newPassword.value != this.reactiveForm.controls.password.value)
        return this.registroFallido('Las contraseñas no coinciden');

      let data = {
        token: this.tokenReset,
        password: this.reactiveForm.controls.password.value,
        email: this.reactiveForm.controls.email.value,
      };
      this.provider.cambiarContrasenia(data).subscribe(res => {
        if (res.status == 'ok') {
          this.formDirective.resetForm();
          setTimeout(function() {
            this.modo = '1';
          }, 1000);
          return this.registroExitoso(res.message);
        } else return this.registroFallido(res.message);
      });
    }
  }

  login() {
    if (
      this.reactiveForm.controls.password.value != '' &&
      this.reactiveForm.controls.email.value != '' &&
      this.reactiveForm.controls.email.value != null &&
      this.reactiveForm.controls.password.value != null
    ) {
      this.iniciar = true;
      this.provider
        .iniciarSesion({
          email: this.reactiveForm.controls.email.value,
          password: this.reactiveForm.controls.password.value,
        })
        .subscribe(
          data => {
            if (data.status == 'ok') {
              this.iniciar = false;
              localStorage.setItem('token', data.data.access_token);
              this.provider.consultarPerfil().subscribe(data => {
                if (data.status == 'ok') {
                  localStorage.setItem('user', JSON.stringify(data.data));
                  if (data.data.rol_id != 3) {
                    this.router.navigateByUrl('/planes');
                  } else {
                    this.router.navigateByUrl('/seguimiento');
                  }
                }
              });
            } else {
              this.credenciales = false;
              this.iniciar = false;
            }
          },
          error => {
            this.credenciales = true;
            this.iniciar = false;
          }
        );
    } else {
      return this.registroFallido('Debe completar todos los campos');
    }
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 1000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 1000,
      panelClass: 'snackbarfail',
    });
  }

  cambiarModo(modo) {
    this.modo = modo;
    this.formDirective.resetForm();
  }

  cerrarErrorCredenciales() {
    this.credenciales = true;
  }
}
