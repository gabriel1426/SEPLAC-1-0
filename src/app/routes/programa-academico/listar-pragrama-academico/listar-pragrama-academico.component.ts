import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-programa-academico-listarPragramaAcademico',
  templateUrl: './listar-pragrama-academico.component.html',
  styleUrls: ['./listar-pragrama-academico.component.scss'],
})
export class ProgramaAcademicoListarPragramaAcademicoComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public facultades: any;
  public departamentos: any;
  public programaAcademicos: any;
  public registrarProgramaAcademico: FormGroup;
  public modificarProgramaAcademico: FormGroup;
  public cargar = false;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarProgramaAcademico = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
    });

    this.modificarProgramaAcademico = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      departamento: ['', [Validators.required]],
    });

    this.tabla();
    this.listarDepartamentos();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();

    this.listarProgramaAcademico();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event

    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarProgramaAcademico() {
    if (this.departamentos == undefined || this.departamentos.length == 0) {
      this.registroFallido('No hay departamentos registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarProgramaAcademicoDialog(item: any) {
    this.item = item;
    this.modificarProgramaAcademico.controls.codigo.setValue(item.codigo);
    this.modificarProgramaAcademico.controls.nombre.setValue(item.nombre);
    this.modificarProgramaAcademico.patchValue({ departamento: item.departamento.id });
    this.dialog.open(this.modificar);
  }

  EliminarProAcademicoDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }
  listarDepartamentos() {
    let url = '/api/departamento';
    this.provider.listar(url).subscribe(
      data => {
        this.departamentos = data.data;
        this.dialog.closeAll();
      },
      error => {}
    );
  }

  listarProgramaAcademico() {
    let url = '/api/programa_academico';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.programaAcademicos = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisProAcademico() {
    if (this.registrarProgramaAcademico.valid) {
      this.cargar = true;
      let data = {
        programasAcademicos: [
          {
            codigo: this.registrarProgramaAcademico.controls.codigo.value,
            nombre: this.registrarProgramaAcademico.controls.nombre.value,
          },
        ],
        departamento_id: this.registrarProgramaAcademico.controls.departamento.value,
      };
      let url = '/api/programa_academico';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarProgramaAcademico.reset();
            this.listarProgramaAcademico();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifiProAcademico(item: any) {
    if (this.modificarProgramaAcademico.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarProgramaAcademico.controls.codigo.value,
        nombre: this.modificarProgramaAcademico.controls.nombre.value,
        departamento_id: this.modificarProgramaAcademico.controls.departamento.value,
      };
      let url = '/api/programa_academico/' + this.item.id;
      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarProgramaAcademico();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarProAcademico() {
    this.cargar = true;
    let url = '/api/programa_academico/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Línea eliminada exitosamente!!');
          this.listarProgramaAcademico();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
