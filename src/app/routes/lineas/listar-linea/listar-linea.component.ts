import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ViewChild, TemplateRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-lineas-listar-linea',
  templateUrl: './listar-linea.component.html',
  styleUrls: ['./listar-linea.component.scss'],
})
export class LineasListarLineaComponent implements AfterViewInit, OnDestroy {
  @ViewChild('agregar', { static: false }) agregar: TemplateRef<any>;
  @ViewChild('modificar', { static: false }) modificar: TemplateRef<any>;
  @ViewChild('eliminar', { static: false }) eliminar: TemplateRef<any>;
  @ViewChild(DataTableDirective, { static: false }) dtElement: DataTableDirective;

  public dtOptions: DataTables.Settings = {};
  public dtTrigger: any = new Subject();
  public item: any;
  public ejes: any;
  public lineas: any;
  public registrarLinea: FormGroup;
  public modificarLinea: FormGroup;
  public cargar = false;

  constructor(
    private provider: ServicesAppService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.registrarLinea = this.fb.group({
      codigo: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      eje: ['', [Validators.required]],
    });

    this.modificarLinea = this.fb.group({
      codigo: [{ value: '', disabled: true }, [Validators.required]],
      nombre: ['', [Validators.required]],
      descripcion: [{ value: '', disabled: false }, [Validators.required]],
      eje: ['', [Validators.required]],
    });
    this.tabla();
    this.listarLinea();
    this.listarEjes();
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  tabla(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',

      language: {
        emptyTable: 'No hay datos disponibles en la tabla',
        info: 'Mostrando  _START_ a _END_ de _TOTAL_ entradas',
        infoEmpty: 'Mostrando  0 a 0 de 0 entradas',
        infoFiltered: '(filtrado de _MAX_ total entradas)',
        infoPostFix: '',
        thousands: ',',
        lengthMenu: 'Mostrar _MENU_ entradas',
        loadingRecords: 'Cargando...',
        processing: 'Procesando...',
        search: 'Buscar:',
        zeroRecords: 'No se encontraron registros coincidentes',
        paginate: {
          first: 'Primero',
          last: 'Último',
          next: 'Sig.',
          previous: 'Ant.',
        },
        aria: {
          sortAscending: ': activar para ordenar la columna ascendente',
          sortDescending: ': activar para ordenar la columna descendente',
        },
      },
    };
  }
  agregarLinea() {
    if (this.ejes == undefined || this.ejes.length == 0) {
      this.registroFallido('No hay ejes estratégicos registrados');
    } else {
      this.dialog.open(this.agregar);
    }
  }

  modificarLineaDialog(item: any) {
    this.item = item;
    this.modificarLinea.controls.codigo.setValue(item.codigo);
    this.modificarLinea.controls.codigo.disable;
    this.modificarLinea.controls.nombre.setValue(item.nombre);
    this.modificarLinea.controls.descripcion.setValue(item.descripcion);
    this.modificarLinea.patchValue({ eje: item.eje_id });
    this.dialog.open(this.modificar);
  }

  EliminarLineaDialog(item: any) {
    this.item = item;
    this.dialog.open(this.eliminar);
  }

  registroExitoso(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarsusses',
    });
  }
  registroFallido(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  listarEjes() {
    let url = '/api/eje';
    this.provider.listar(url).subscribe(
      data => {
        this.ejes = data.data;
      },
      error => {}
    );
  }

  listarLinea() {
    let url = '/api/linea';
    this.provider.listar(url).subscribe(
      data => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.lineas = data.data;
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      },
      error => {}
    );
  }

  regisLinea() {
    if (this.registrarLinea.valid) {
      this.cargar = true;
      let data = {
        lineas: [
          {
            codigo: this.registrarLinea.controls.codigo.value,
            nombre: this.registrarLinea.controls.nombre.value,
            descripcion: this.registrarLinea.controls.descripcion.value,
          },
        ],
        eje_id: this.registrarLinea.controls.eje.value,
      };

      let url = '/api/linea';
      this.provider.registrar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Registro exitoso!!');
            this.registrarLinea.reset();
            this.listarLinea();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Registro fallido!!');
        }
      );
    }
  }

  modifyLine(item: any) {
    if (this.modificarLinea.valid) {
      this.cargar = true;
      let data = {
        codigo: this.modificarLinea.controls.codigo.value,
        nombre: this.modificarLinea.controls.nombre.value,
        descripcion: this.modificarLinea.controls.descripcion.value,
        eje_id: this.modificarLinea.controls.eje.value,
      };
      let url = '/api/linea/' + this.item.id;

      this.provider.modifcar(url, data).subscribe(
        data => {
          if (data.status == 'ok') {
            this.registroExitoso('Modificación exitosa!!');
            this.listarLinea();
            this.cargar = false;
            this.dialog.closeAll();
          } else {
            this.cargar = false;
            this.registroFallido(data.message);
          }
        },
        error => {
          this.cargar = false;
          this.registroFallido('Modificacion fallida!!');
        }
      );
    }
  }
  eliminarLinea() {
    this.cargar = true;
    let url = '/api/linea/' + this.item.id;
    this.provider.eliminar(url).subscribe(
      data => {
        if (data.status == 'ok') {
          this.registroExitoso('Línea eliminada exitosamente!!');
          this.listarLinea();
          this.cargar = false;
          this.dialog.closeAll();
        } else {
          this.cargar = false;
          this.registroFallido(data.message);
        }
      },
      error => {
        this.cargar = false;
        this.registroFallido('Fallo al eliminar');
      }
    );
  }

  cerrarModal() {
    this.dialog.closeAll();
  }
}
