import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LineasListarLineaComponent } from './listar-linea/listar-linea.component';

const routes: Routes = [{ path: '', component: LineasListarLineaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LineasRoutingModule {}
