import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '@env/environment';

import { AdminLayoutComponent } from '../theme/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from '../theme/auth-layout/auth-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './sessions/login/login.component';
import { RegisterComponent } from './sessions/register/register.component';

import { AuthGuardDocentesServiceService } from '../core/AuthGuard/auth-guard-docentes-service.service';
import { AuthGuardAdministrativoService } from '../core/AuthGuard/auth-guard-administrativo.service';
import { AuthGuardDashboardService } from '../core/AuthGuard/auth-guard-dashboard.service';
import { AuthGuardDirectorProgramaService } from '../core/AuthGuard/auth-guard-director-programa.service';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,

    children: [
      { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
      /*{
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuardDashboardService],
        data: { title: 'Dashboard', titleI18n: 'dashboard' },
      }*/
      
      {
        path: 'sessions',
        loadChildren: () => import('./sessions/sessions.module').then(m => m.SessionsModule),
        data: { title: 'Sessions', titleI18n: 'Sessions' },
      },
      {
        path: 'docentes',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () => import('./docentes/docentes.module').then(m => m.DocentesModule),
      },
      {
        path: 'administrativos',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./administrativos/administrativos.module').then(m => m.AdministrativosModule),
      },
      {
        path: 'ejes_estrategicos',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./ejes-estrategicos/ejes-estrategicos.module').then(
            m => m.EjesEstrategicosModule
          ),
      },
      {
        path: 'lineas',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () => import('./lineas/lineas.module').then(m => m.LineasModule),
      },
      {
        path: 'programas',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () => import('./programas/programas.module').then(m => m.ProgramasModule),
      },
      {
        path: 'planes',
        canActivate: [AuthGuardDirectorProgramaService],
        loadChildren: () => import('./planes/planes.module').then(m => m.PlanesModule),
      },
      {
        path: 'proyectos',
        canActivate: [AuthGuardDashboardService],
        loadChildren: () => import('./proyectos/proyectos.module').then(m => m.ProyectosModule),
      },
      {
        path: 'actividades',
        canActivate: [AuthGuardDashboardService],
        loadChildren: () =>
          import('./actividades/actividades.module').then(m => m.ActividadesModule),
      },
      {
        path: 'seguimiento',
        canActivate: [AuthGuardDocentesServiceService],
        loadChildren: () =>
          import('./seguimiento/seguimiento.module').then(m => m.SeguimientoModule),
      },
      {
        path: 'facultad',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () => import('./facultad/facultad.module').then(m => m.FacultadModule),
      },
      {
        path: 'departamento',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./departamento/departamento.module').then(m => m.DepartamentoModule),
      },
      {
        path: 'programaAcademico',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./programa-academico/programa-academico.module').then(
            m => m.ProgramaAcademicoModule
          ),
      },
      {
        path: 'indicadores',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./indicadores/indicadores.module').then(m => m.IndicadoresModule),
      },
      {
        path: 'recursos',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () => import('./recursos/recursos.module').then(m => m.RecursosModule),
      },
      {
        path: 'usuarios',
        loadChildren: () => import('./usuarios/usuarios.module').then(m => m.UsuariosModule),
      },
      {
        path: 'director-programa',
        canActivate: [AuthGuardAdministrativoService],
        loadChildren: () =>
          import('./director-programa/director-programa.module').then(
            m => m.DirectorProgramaModule
          ),
      },
      {
        path: 'reportes',
        loadChildren: () => import('./reportes/reportes.module').then(m => m.ReportesModule),
      },
      {
        path: 'download',
        loadChildren: () => import('./download/download.module').then(m => m.DownloadModule),
      },
      {
        path: 'sessions/perfil',
        loadChildren: () => import('./download/download.module').then(m => m.DownloadModule),
      },
    ],
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        data: { title: 'Login', titleI18n: 'Login' },
      },
      {
        path: 'register',
        component: RegisterComponent,
        data: { title: 'Register', titleI18n: 'Register' },
      },
      {
        path: 'perfil',
        component: RegisterComponent,
        data: { title: 'Perfil', titleI18n: 'Perfil' },
      },
    ],
  },
  { path: '**', redirectTo: 'planes' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
    }),
  ],
  exports: [RouterModule],
})
export class RoutesRoutingModule {}
