import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ServicesAppService {
  public urlBase = 'http://seplac.cpsw.ingsistemasufps.co/apiseplac/public';
  //public urlBase = 'https://apiseplac-github.herokuapp.com';
  //public urlBase = 'https://7d2d4dd7.ngrok.io';

  private countdownEndSource = new Subject<void>();

  constructor(private http: HttpClient) {}

  /*
  /
  /Servicio de Inicio de sesion
  /
  */

  obtenerToken(): string {
    return localStorage.getItem('token');
  }

  obtenerUser(): string {
    this.countdownEndSource.next(JSON.parse(localStorage.getItem('user')));
    return JSON.parse(localStorage.getItem('user'));
  }

  getUsuario$(): Observable<void> {
    return this.countdownEndSource.asObservable();
  }

  obtenerUrl(): string {
    return this.urlBase;
  }

  iniciarSesion(data: any): Observable<any> {
    return this.http.post(this.urlBase + '/api/auth/login', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  solicitarCambioContrasenia(data: any): Observable<any> {
    return this.http.post(this.urlBase + '/api/password', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  cambiarContrasenia(data): Observable<any> {
    return this.http.post(this.urlBase + '/api/password/change', data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  consultarPerfil(): Observable<any> {
    var token = this.obtenerToken();
    return this.http.post(
      this.urlBase + '/api/auth/me?token=' + token,
      {},
      { headers: { 'Content-Type': 'application/json' } }
    );
  }

  //
  //
  // Gestión de ejes
  //
  //

  // Metodo utilizada para consumir el servicio de listar <T>
  listar(url: any): Observable<any> {
    var token = this.obtenerToken();
    return this.http.get(this.urlBase + url + '?token=' + token, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  // Metodo utilizada para consumir el servicio de registrar <T>
  registrar(url, data: any): Observable<any> {
    var token = this.obtenerToken();
    return this.http.post(this.urlBase + url + '?token=' + token, data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  // Metodo utilizada para consumir el servicio de registrar <T>
  registrarMultiParte(url, data: any): Observable<any> {
    var token = this.obtenerToken();
    return this.http.post(this.urlBase + url + '?token=' + token, data);
  }

  // Metodo utilizada para consumir el servicio de modificar <T>
  modifcar(url, data: any): Observable<any> {
    var token = this.obtenerToken();

    return this.http.put(this.urlBase + url + '?token=' + token, data, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  // Metodo utilizada para consumir el servicio de modificar <T>
  modifcarMultiParte(url, data: any): Observable<any> {
    var token = this.obtenerToken();
    return this.http.post(this.urlBase + url + '?token=' + token, data);
  }

  // Metodo utilizada para consumir el servicio de eliminar <T>
  eliminar(url): Observable<any> {
    var token = this.obtenerToken();
    return this.http.delete(this.urlBase + url + '?token=' + token, {
      headers: { 'Content-Type': 'application/json' },
    });
  }
  // Metodo utilizado para cargar archivos
  public upload(formData) {
    var token = this.obtenerToken();
    return this.http.post<any>(
      this.urlBase + '/api/plan/proyecto/seguimiento/comentario/evidencia' + '?token=' + token,
      formData
    );
  }
}
