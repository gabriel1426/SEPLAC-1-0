import { ServicesAppService } from '../../core/services/services-app.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  Route,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardAdministrativoService implements CanActivate {
  constructor(private provider: ServicesAppService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | import('@angular/router').UrlTree
    | Observable<boolean | import('@angular/router').UrlTree>
    | Promise<boolean | import('@angular/router').UrlTree> {
    let token = this.provider.obtenerToken();
    let usuario: any = this.provider.obtenerUser();

    if (token != null && (usuario.rol_id == 1 || usuario.rol_id == 2)) {
      return true;
    } else {
      if (token != null) {
        let user:any = this.provider.obtenerUser();
        if (user != null) {
          if (user.rol_id != 3) {
            this.router.navigateByUrl('/planes');
          } else {
            this.router.navigateByUrl('/seguimiento');
          }
        }
      } else {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.router.navigate(['/auth/login']);
      }
    }
  }
}
