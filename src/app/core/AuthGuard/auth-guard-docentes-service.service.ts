import { ServicesAppService } from '../../core/services/services-app.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  Route,
} from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
@Injectable()
export class AuthGuardDocentesServiceService implements CanActivate {
  constructor(private provider: ServicesAppService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | import('@angular/router').UrlTree
    | Observable<boolean | import('@angular/router').UrlTree>
    | Promise<boolean | import('@angular/router').UrlTree> {
    let token = this.provider.obtenerToken();
    let usuario: any = this.provider.obtenerUser();
    if (
      token != null && usuario.rol_id == 3 
    ) {
      return true;
    } else {
      if (token != null) {
        this.router.navigate(['/dashboard']);
      } else {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.router.navigate(['/auth/login']);
      }
    }
  }
}
