import { ServicesAppService } from '../../core/services/services-app.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  Route,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardDashboardService implements CanActivate {
  constructor(private provider: ServicesAppService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | import('@angular/router').UrlTree
    | Observable<boolean | import('@angular/router').UrlTree>
    | Promise<boolean | import('@angular/router').UrlTree> {
    let token = this.provider.obtenerToken();
    if (token != null) {
      return true;
    } else {
      localStorage.removeItem('token');
       localStorage.removeItem('user');
      this.router.navigate(['/auth/login']);
    }
  }
}
