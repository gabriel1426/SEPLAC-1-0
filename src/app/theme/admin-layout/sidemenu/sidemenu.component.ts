import { Component, Input } from '@angular/core';
import { MenuService } from '@core';
import { ServicesAppService } from '../../../core/services/services-app.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
})
export class SidemenuComponent {
  // NOTE: Ripple effect make page flashing on mobile
  @Input() ripple = true;

  menus = this.menuService.getAll();
  tipo:any

  constructor(private menuService: MenuService,private provider: ServicesAppService,){
    this.provider.getUsuario$().subscribe(
      data => {
        this.tipo=data['rol_id'];
      }
    );
  }

  // Delete empty value in array
  filterStates(states: string[]) {
    return states.filter(item => item && item.trim());
  }
}
