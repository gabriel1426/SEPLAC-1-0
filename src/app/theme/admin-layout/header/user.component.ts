import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesAppService } from '../../../core/services/services-app.service';
import * as screenfull from 'screenfull';

@Component({
  selector: 'app-user',
  template: `
    <a mat-button href="javascript:void(0)" [matMenuTriggerFor]="menu">
      <mat-icon>settings</mat-icon>
    </a>
    <mat-menu #menu="matMenu">
    <a (click)="toggleFullscreen()" mat-menu-item>
        <mat-icon>fullscreen</mat-icon>
        <span>Pantalla completa</span>
      </a>
      <a routerLink="/sessions/perfil" mat-menu-item>
        <mat-icon>account_circle</mat-icon>
        <span>Perfil</span>
      </a>
      <a (click)="logout()" mat-menu-item>
        <mat-icon>exit_to_app</mat-icon>
        <span>Salir</span>
      </a>
    </mat-menu>
  `,
})
export class UserComponent {
  private get screenfull(): screenfull.Screenfull {
    return screenfull as screenfull.Screenfull;
  }
  constructor(private router: Router,private provider: ServicesAppService,) {
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigateByUrl('/auth/login');
  }
   // TODO:
   toggleFullscreen() {
    if (this.screenfull.enabled) {
      this.screenfull.toggle();
    }
  }
}
