import { Component } from '@angular/core';

@Component({
  selector: 'app-branding',
  template: `
    <a class="matero-branding" href="#/dashboard">
      <img src="./assets/images/logoUFPS.png" class="matero-branding-logo-expanded" alt="" />
      <span class="matero-branding-name">SEPLAC</span>
    </a>
  `,
})
export class BrandingComponent {}
