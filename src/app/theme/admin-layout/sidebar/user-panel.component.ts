import { Component } from '@angular/core';
import { ServicesAppService } from '../../../core/services/services-app.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-panel',
  template: `
    <div
      class="matero-user-panel p-y-16 b-t-1 b-b-1"
      fxLayout="column"
      fxLayoutAlign="center center"
    >
      <img
        class="matero-user-panel-avatar m-b-8 r-full"
        src="assets/images/avataaars.png"
        alt="avatar"
        width="64"
      />
      <h4 class="matero-user-panel-name m-t-0 m-b-8 f-w-400">{{ nombre }}</h4>
      <h5 class="matero-user-panel-email m-t-0 m-b-8 f-w-400">{{ correo }}</h5>
      <div class="matero-user-panel-icons text-nowrap"></div>
    </div>
  `,
})
export class UserPanelComponent {
  public correo = '';
  public nombre = '';

  constructor(
    private snackBar: MatSnackBar,
    private provider: ServicesAppService,
    private router: Router
  ) {
    this.consultarPefil();
  }
  consultarPefil() {
    this.provider.consultarPerfil().subscribe(
      data => {
        if (data.status == 'ok') {
          this.nombre = data.data.name;
          this.correo = data.data.email;
          localStorage.setItem('user', JSON.stringify(data.data));
          this.provider.obtenerUser();
        }
      },
      error => {
        this.logout();
      }
    );
  }
  cerrarSesion(mensaje: any) {
    this.snackBar.open(mensaje, '', {
      duration: 3000,
      panelClass: 'snackbarfail',
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.cerrarSesion('Sesión terminada');
    this.router.navigateByUrl('/auth/login');
  }
}
